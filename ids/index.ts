const readableChars = "bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ123456789";

/**
 * readableId returns an ID that is free from vowels (to prevent swear words)
 * and ambiguous characters.
 */
export function readableId(length: number): string {
  const result = new Array(length);

  for (let i = 0; i < length; i++) {
    result[i] = readableChars[
      Math.floor(Math.random() * readableChars.length)
    ];
  }

  return result.join("");
}
