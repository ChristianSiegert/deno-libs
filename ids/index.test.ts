import { readableId } from "./index.ts";
import { assertMatch } from "../deps.ts";

Deno.test(readableId.name, () => {
  for (let length = 1; length <= 30; length++) {
    const pattern = new RegExp(
      `^[bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ123456789]{${length}}$`,
    );

    for (let i = 0; i < 100; i++) {
      assertMatch(readableId(length), pattern);
    }
  }
});
