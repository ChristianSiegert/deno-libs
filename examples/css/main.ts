import { App, BaseContext, HttpMethod, typeRoutes } from "@/app/mod.ts";
import { ContentType } from "@/http/mod.ts";

const { GET } = HttpMethod;

const app = new App({
  createContext: (baseContext) => baseContext,
  routes: typeRoutes()({
    home: {
      handler: async (_context: BaseContext): Promise<Response> => {
        return new Response(await Deno.readTextFile("main.html"), {
          headers: { "content-type": ContentType.TextHtml },
        });
      },
      methods: GET,
      pattern: new URLPattern({ pathname: "/" }),
      url: () => "/",
    },
    css: {
      handler: async (context: BaseContext) => {
        const filename = context.urlPatternResult?.pathname.groups.filename;
        return new Response(await Deno.readTextFile(`../../css/${filename}`), {
          headers: { "content-type": ContentType.TextCss },
        });
      },
      methods: GET,
      pattern: new URLPattern({ pathname: "/css/:filename" }),
      url: (filename: string) => `/css/${filename}`,
    },
  }),
});

Deno.serve({ hostname: "0.0.0.0", port: 8080 }, app.handler);
