View how HTML elements are styled with `base.css` and `theme.css`:

```
cd examples/css
deno run --allow-net=0.0.0.0:8080 --allow-read --check main.ts
deno run --allow-net=0.0.0.0:8080 --allow-read --check --watch main.ts
```
