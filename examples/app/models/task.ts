export type Task = {
  dateCreated: Date;
  text: string;
};

export const tasks: Task[] = [{
  dateCreated: new Date("2022-07-04T11:33:23.860Z"),
  text: "Buy milk",
}, {
  dateCreated: new Date("2022-07-04T11:35:12.877Z"),
  text: "Change light bulb",
}];
