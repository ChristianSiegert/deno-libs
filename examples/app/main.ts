import { App } from "@/app/mod.ts";
import { createU, UtilityStylesheet } from "@/css/mod.ts";

import { type Context } from "@/examples/app/context.ts";
import { routes } from "@/examples/app/routes.ts";
import { translationsEnUs } from "@/examples/app/translations/en-us.ts";

let requestCounter = 0;

// Serve app with custom context
const app = new App<Context>({
  createContext: (baseContext): Context => {
    requestCounter += 1;
    const stylesheet = new UtilityStylesheet();
    return {
      ...baseContext,
      counter: requestCounter,
      notifications: [],
      stylesheet,
      t: translationsEnUs,
      u: createU(stylesheet),
    };
  },
  routes,
});

app.onHandlerExit = (_context: Context, response: Response) => {
  const { headers } = response;
  const isTextHtml = headers.get("content-type")?.startsWith("text/html");

  if (isTextHtml && !headers.has("content-security-policy")) {
    headers.set(
      "content-security-policy",
      "default-src 'self';" +
        `script-src 'self' 'unsafe-inline';` +
        `style-src 'self' 'unsafe-inline';` +
        "base-uri 'self';" +
        "form-action 'self';" +
        "frame-ancestors 'self';",
    );
  }
  return response;
};

Deno.serve({ hostname: "0.0.0.0", port: 80 }, app.handler);
