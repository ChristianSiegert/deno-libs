import { button, Component, div, form, input, label } from "@/html/mod.ts";
import { ContentType } from "@/http/mod.ts";
import { Form, FormField } from "@/validation/mod.ts";

import { type Context } from "@/examples/app/context.ts";
import mainTemplate from "@/examples/app/pages/main.template.ts";

export class HomeForm extends Form<Context> {
  age = new FormField({
    name: "age",
    validators: [{
      fn: ({ value }) => !!value,
      message: () => this.context.t.validation.required,
    }, {
      fn: (field) => {
        const age = Number.parseInt(field.value);
        return age >= 0 && age <= 120;
      },
      message: () => this.context.t.home.formAgeInvalid(0, 120),
    }],
  });

  name = new FormField({
    name: "name",
    validators: [{
      fn: ({ value }) => !!value,
      message: () => this.context.t.validation.required,
    }, {
      fn: ({ value }) => value.length >= 2 && value.length <= 20,
      message: () => this.context.t.validation.requiredLength(2, 20),
    }],
  });
}

function template(context: Context, options: { form: HomeForm }): Component {
  const { t, u } = context;
  return new Component([
    form({
      class: u`max-width:400px mx:auto`,
      method: "post",
    }, [
      div([
        label({
          class: u`font-weight:700`,
          for: "name",
        }, t.name),
        input({
          class: {
            [u`mt-1 width:100%`]: true,
            [u`border-color:[red !important]`]: !!options.form.age.message,
          },
          id: "name",
          name: options.form.name.name,
          placeholder: t.home.enterName,
          type: "text",
          value: options.form.name.value,
        }),
        div({
          if: options.form.name.message,
          class: u`color:red font-size:12px mt-1`,
        }, options.form.name.message),
      ]),
      div({ class: u`mt-4` }, [
        label({
          class: u`font-weight:700`,
          for: "age",
        }, t.age),
        input({
          class: {
            [u`mt-1 width:100%`]: true,
            [u`border-color:[red !important]`]: !!options.form.age.message,
          },
          id: "age",
          name: options.form.age.name,
          placeholder: t.home.enterAge,
          type: "text",
          value: options.form.age.value,
        }),
        div({
          if: options.form.age.message,
          class: u`color:red font-size:12px mt-1`,
        }, options.form.age.message),
      ]),
      div({ class: u`mt-4` }, [
        button(t.action.submit),
      ]),
      div({
        if: options.form.isValid,
        class: u`mt-4`,
      }, `${options.form.name.value} is ${options.form.age.value} years old`),
    ]),
  ]);
}

export default async function handler(context: Context): Promise<Response> {
  const { t } = context;
  const form = new HomeForm(context);

  if (context.request.method === "POST") {
    form.valuesFrom(await context.request.formData());
    await form.validate();
  }

  return new Response(
    await mainTemplate(context, {
      content: template(context, { form }),
      title: t.home.pageTitle,
    }),
    {
      headers: {
        "content-type": ContentType.TextHtml,
        "x-counter": context.counter.toString(),
      },
    },
  );
}
