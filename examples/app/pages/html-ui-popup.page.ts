import { css } from "@/css/mod.ts";
import { Component, csPopup, div, h1, summary } from "@/html/mod.ts";
import { ContentType } from "@/http/http.ts";

import { type Context } from "@/examples/app/context.ts";
import mainTemplate from "@/examples/app/pages/main.template.ts";

function template(_context: Context): Component {
  return new Component([
    h1("Popup"),
    div({ class: "content" }, [
      csPopup({
        button: new Component([
          summary({ class: "popup-button" }, "Click me"),
        ]),
        popup: new Component([
          div({ class: "popup" }, ["The content"]),
        ]),
      }),
      csPopup({
        button: new Component([
          summary({ class: "popup-button" }, "Click me"),
        ]),
        popup: new Component([
          div({ class: "popup" }, ["Popup is aligned to right edge of button"]),
        ]),
        popupAnchor: "right",
      }),
    ]),
  ], { style });
}

export default async function handler(context: Context): Promise<Response> {
  return new Response(
    await mainTemplate(context, {
      content: template(context),
      title: "Popup",
    }),
    { headers: { "content-type": ContentType.TextHtml } },
  );
}

const style = css`
.content {
  display: flex;
  justify-content: space-evenly;
}
.popup-button {
  background: gold;
  cursor: pointer;
  padding: 0.5rem;
  user-select: none;
  width: fit-content;
}
.popup {
  background: #ddd;
  border: 1px solid #d3d3d3;
  border-radius: 5px;
  box-shadow: 0 5px 10px rgb(0 0 0 / 15%);
  font-size: 15px;
  max-width: 200px;
  padding: 0.5rem;
}
@media screen and (prefers-color-scheme: dark) {
  .popup-button {
    color: #333;
  }
  .popup {
    background: #333;
  }
}
`;
