import { Component, h1 } from "@/html/mod.ts";
import { ContentType } from "@/http/http.ts";

import { type Context } from "@/examples/app/context.ts";
import mainTemplate from "@/examples/app/pages/main.template.ts";

function template(_context: Context): Component {
  return new Component([
    h1("CsNotifications"),
  ]);
}

export default async function handler(context: Context): Promise<Response> {
  context.notifications.push({
    title: "Lorem Ipsum",
    text: "Enim dolores pariatur qui facere",
  }, {
    title: "Lorem Ipsum",
    text: "Enim dolores pariatur qui facere",
    type: "warning",
  });

  return new Response(
    await mainTemplate(context, {
      content: template(context),
      title: "CsNotifications",
    }),
    { headers: { "content-type": ContentType.TextHtml } },
  );
}
