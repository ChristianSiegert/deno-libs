import { css } from "@/css/mod.ts";
import { Component, div, h1 } from "@/html/mod.ts";
import { ContentType } from "@/http/mod.ts";

import { renderTask } from "@/examples/app/components/task.ts";
import { type Context } from "@/examples/app/context.ts";
import { type Task, tasks } from "@/examples/app/models/task.ts";
import mainTemplate from "@/examples/app/pages/main.template.ts";

function template(context: Context, tasks: Task[]): Component {
  const { t } = context;
  return new Component([
    h1(t.tasks.heading),
    div({
      class: "tasks",
    }, tasks.map((task) => renderTask(context, task))),
    div({
      class: "tasks__count",
    }, t.tasks.count(tasks.length)),
  ], { style });
}

export default async function handler(context: Context): Promise<Response> {
  const { t } = context;
  return new Response(
    await mainTemplate(context, {
      content: template(context, tasks),
      title: t.tasks.pageTitle,
    }),
    {
      headers: {
        "content-type": ContentType.TextHtml,
        "x-counter": context.counter.toString(),
      },
    },
  );
}

const style = css`
.tasks {
  margin-top: 0.5rem;
}
.tasks__count {
  margin-top: 0.5rem;
}
`;
