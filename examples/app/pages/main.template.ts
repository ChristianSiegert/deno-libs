import { css } from "@/css/css.ts";
import {
  body,
  Component,
  csNotifications,
  head,
  html,
  meta,
  script,
  style,
  title,
} from "@/html/mod.ts";

import { type Context } from "@/examples/app/context.ts";

const [baseCss, themeCss] = await Promise.all([
  Deno.readTextFile("css/base.min.css"),
  Deno.readTextFile("css/theme.min.css"),
]);

export default async function mainTemplate(context: Context, options: {
  content: Component;
  title: string;
}): Promise<string> {
  const { stylesheet } = context;

  const bodyComponent = new Component([
    body([
      await csNotifications({ notifications: context.notifications }),
      options.content,
    ]),
  ]);

  const styles = bodyComponent.getStyles();
  const scripts = bodyComponent.getScripts();

  return "<!DOCTYPE html>" +
    html({ lang: "en-us" }, [
      head([
        meta({ charset: "utf-8" }),
        meta({
          name: "viewport",
          content: "width=device-width, initial-scale=1",
        }),
        title(options.title),
        style([
          stylesheet.toString(),
          baseCss,
          themeCss,
          mainCss,
          ...styles,
        ]),
        script({ if: scripts.length }, scripts),
      ]),
      bodyComponent,
    ]).toString();
}

const mainCss = css`
body {
  margin: 1.5rem;
}
`;
