import { css } from "@/css/mod.ts";
import { Component, div, time } from "@/html/mod.ts";

import { type Context } from "@/examples/app/context.ts";
import { type Task } from "@/examples/app/models/task.ts";

const dtf = new Intl.DateTimeFormat("en-us", { dateStyle: "long" });

export function renderTask(_context: Context, task: Task): Component {
  return new Component([
    div({ class: "task" }, [
      div({ class: "task__text" }, task.text),
      time({
        datetime: task.dateCreated.toISOString(),
      }, dtf.format(task.dateCreated)),
    ]),
  ], { style });
}

const style = css`
.task {
  background-color: hsl(180deg 25% 65%);
  padding: 0.5rem;
}
.task__text {
  font-size: large;
  font-weight: 700;
}
@media screen and (prefers-color-scheme: dark) {
  .task {
    background-color: hsl(180deg 25% 25%);
  }
}
`;
