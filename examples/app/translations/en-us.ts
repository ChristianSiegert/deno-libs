export const translationsEnUs = {
  age: "Age",
  name: "Name",

  action: {
    submit: "Submit",
  },

  home: {
    enterAge: "Enter the person’s age",
    enterName: "Enter a person’s name",
    formAgeInvalid: (minAge: number, maxAge: number) =>
      `Age must be between ${minAge} and ${maxAge} years`,
    pageTitle: "Home Page",
  },

  tasks: {
    count: (count: number) => `Count: ${count}`,
    heading: "Tasks",
    pageTitle: "Tasks",
  },

  validation: {
    requiredLength: (min: number, max: number) =>
      `Must be between ${min} and ${max} characters long`,
    required: "Required",
  },
};
