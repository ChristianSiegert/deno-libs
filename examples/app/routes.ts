import { HttpMethod, typeRoutes } from "@/app/mod.ts";

import { type Context } from "@/examples/app/context.ts";
import home from "@/examples/app/pages/home.page.ts";
import ionIconsV4 from "@/examples/app/pages/html-ui-ion-icons-v4.page.ts";
import ionIconsV7 from "@/examples/app/pages/html-ui-ion-icons-v7.page.ts";
import notifications from "@/examples/app/pages/html-ui-notifications.page.ts";
import popup from "@/examples/app/pages/html-ui-popup.page.ts";
import tasks from "@/examples/app/pages/tasks.page.ts";

const { GET, POST } = HttpMethod;

export const routes = typeRoutes<Context>()({
  home: {
    handler: home,
    methods: GET | POST,
    pattern: "/",
    url: () => "/",
  },
  htmlUiIonIconV4: {
    handler: ionIconsV4,
    methods: GET,
    pattern: "/ion-icons-v4",
    url: () => "/ion-icon-v4",
  },
  htmlUiIonIconV7: {
    handler: ionIconsV7,
    methods: GET,
    pattern: "/ion-icons-v7",
    url: () => "/ion-icon-v7",
  },
  htmlUiPopup: {
    handler: popup,
    methods: GET,
    pattern: "/popup",
    url: () => "/popup",
  },
  notificatons: {
    handler: notifications,
    methods: GET,
    pattern: "/cs-notifications",
    url: () => "/cs-notifications",
  },
  tasks: {
    handler: tasks,
    methods: GET,
    pattern: "/tasks",
    url: () => "/tasks",
  },
});
