import { type BaseContext } from "@/app/mod.ts";
import { createU, type Stylesheet } from "@/css/mod.ts";
import { type CsNotification } from "@/html/mod.ts";

import { type translationsEnUs } from "@/examples/app/translations/en-us.ts";

export type Context = BaseContext & {
  counter: number;
  notifications: CsNotification[];
  stylesheet: Stylesheet;
  t: typeof translationsEnUs;
  u: ReturnType<typeof createU>;
};
