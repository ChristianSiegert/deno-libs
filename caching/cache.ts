/**
 * Returns a function that, when called, executes `arg`, and caches and returns its result.
 * Subsequent calls return the cached result of `arg` without executing `arg` again.
 */
export function lazyCache<T>(arg: () => T): () => T {
  let isCached = false;
  let result: T;

  return () => {
    if (isCached) return result;
    result = arg();
    isCached = true;
    return result;
  };
}
