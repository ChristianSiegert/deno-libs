import { lazyCache } from "@/caching/cache.ts";
import { assertEquals } from "@/deps.ts";

Deno.test(lazyCache.name, async () => {
  let counter = 0;
  let counterAsync = 0;

  function count(value: number) {
    counter += value;
    return counter;
  }

  async function countAsync(value: number) {
    await Promise.resolve();
    counterAsync += value;
    return counterAsync;
  }

  // arg is sync function
  const result = lazyCache(() => count(3));
  assertEquals(counter, 0);

  assertEquals(result(), 3);
  assertEquals(counter, 3);

  assertEquals(result(), 3);
  assertEquals(counter, 3);

  // arg is async function
  const resultAsync = lazyCache(async () => await countAsync(3));
  assertEquals(counterAsync, 0);

  assertEquals(await resultAsync(), 3);
  assertEquals(counterAsync, 3);

  assertEquals(await resultAsync(), 3);
  assertEquals(counterAsync, 3);
});
