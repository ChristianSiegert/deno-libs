import { assertEquals, assertRejects } from "@/deps.ts";

import { _computeBackoffInMs, retry } from "./retry.ts";

Deno.test(retry.name, async () => {
  let attempt = 0;

  function fail4of5Attempts(): boolean {
    const maxAttempts = 5;
    attempt += 1;
    if (attempt < maxAttempts) {
      throw new Error(`failed intentionally in attempt ${attempt} of ${maxAttempts}`);
    }
    return true;
  }

  function failAllTries(): void {
    throw new Error(`failed intentionally`);
  }

  const result = await retry(() => fail4of5Attempts(), { maxAttempts: 5, warn: false });
  assertEquals(result, true);

  await assertRejects(async () => await retry(() => failAllTries(), { warn: false }));
});

Deno.test(_computeBackoffInMs.name, () => {
  let backoff = _computeBackoffInMs({
    attempt: 1,
    maxBackoffInMs: 10_000,
    minBackoffInMs: 1_000,
    multiplier: 2,
  });
  assertEquals(backoff, 1000);

  backoff = _computeBackoffInMs({
    attempt: 2,
    maxBackoffInMs: 10_000,
    minBackoffInMs: 1_000,
    multiplier: 2,
  });
  assertEquals(backoff, 2000);

  backoff = _computeBackoffInMs({
    attempt: 3,
    maxBackoffInMs: 10_000,
    minBackoffInMs: 1_000,
    multiplier: 2,
  });
  assertEquals(backoff, 4000);

  backoff = _computeBackoffInMs({
    attempt: 4,
    maxBackoffInMs: 10_000,
    minBackoffInMs: 1_000,
    multiplier: 2,
  });
  assertEquals(backoff, 8000);

  backoff = _computeBackoffInMs({
    attempt: 5,
    maxBackoffInMs: 10_000,
    minBackoffInMs: 1_000,
    multiplier: 2,
  });
  assertEquals(backoff, 10000);
});
