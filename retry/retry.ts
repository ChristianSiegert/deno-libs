import { delay } from "@/deps.ts";

export type RetryOptions = {
  /** Maximum number of attemps. */
  maxAttempts?: number;
  /** Maximum duration in milliseconds to wait after the previous attempt. */
  maxBackoffInMs?: number;
  /** Minimum duration in milliseconds to wait after the first attempt. */
  minBackoffInMs?: number;
  /** Number with which to multiply the backoff duration from the previous attempt. */
  multiplier?: number;
  /** Whether to log a warning to the console if an attempt failed. */
  warn?: boolean;
};

/**
 * Calls the provided function until it completes without throwing, but not more than `maxAttempts`.
 * If the function does not succeed within the maximum number of attempts, the last value thrown by
 * the function is re-thrown.
 */
export async function retry<T>(fn: () => T, options?: RetryOptions): Promise<T> {
  const maxAttempts = options?.maxAttempts ?? 3;
  const maxBackoffInMs = options?.maxBackoffInMs ?? 1000;
  const minBackoffInMs = options?.minBackoffInMs ?? 100;
  const multiplier = options?.multiplier ?? 2;
  const warn = options?.warn ?? true;

  const start = Date.now();
  const retryId = crypto.randomUUID();
  let _error: unknown;

  for (let attempt = 1; attempt <= maxAttempts; attempt++) {
    try {
      return await fn();
    } catch (error: unknown) {
      _error = error;
      if (warn) {
        console.warn({
          retryId,
          attempt,
          timeTotal: `${Date.now() - start} ms`,
          error,
        });
      }

      if (attempt < maxAttempts) {
        const backoffInMs = _computeBackoffInMs({
          attempt,
          maxBackoffInMs,
          minBackoffInMs,
          multiplier,
        });
        await delay(backoffInMs);
      }
    }
  }

  throw _error;
}

export function _computeBackoffInMs(
  options: Required<Omit<RetryOptions, "maxAttempts" | "warn">> & { attempt: number },
): number {
  const backoffInMs = options.minBackoffInMs * options.multiplier ** (options.attempt - 1);
  return Math.min(options.maxBackoffInMs, backoffInMs);
}
