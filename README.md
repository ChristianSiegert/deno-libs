# deno-libs

deno-libs is a collection of classes, functions, etc. that I use in my [Deno](https://deno.com)
projects.

Compatible with Deno version ≥ 1.37.0 and ≤ 2.1.6

## `deno-libs/app`

**HTTP server with router and context.**

**Quick Start**

1. Define the app context object, based on `BaseContext`.
2. Define routes. Each route has a `URLPattern` that is compared to the incoming request’s URL, a
   handler function that is executed when the route matches the request URL, and a url function that
   returns the route’s URL so you can create rock solid links to your app’s pages.
3. Create your `App` instance and start accepting requests!

```ts
// Example

import {
  App,
  type BaseContext,
  typeRoutes,
} from "https://gitlab.com/ChristianSiegert/deno-libs/-/raw/0.39.0/app/mod.ts";

type Context = BaseContext & {
  customProperty1: string;
};

const routes = typeRoutes<Context>()({
  home: {
    handler: (_context: Context) => new Response("Welcome"),
    pattern: new URLPattern({ pathname: "/" }),
    url: () => "/",
  },
  posts: {
    handler: (_context: Context) => new Response("Posts here"),
    pattern: new URLPattern({ pathname: "/posts" }),
    url: (page: number) => "/posts?" + new URLSearchParams({ page: page.toString() }),
  },
});

// Each route allows you to create its url
console.log(routes.posts.url(2)); // /posts?page=2

const app = new App<Context>({
  createContext: (baseContext: BaseContext): Context => ({
    ...baseContext,
    customProperty1: "whatever you want",
  }),
  routes,
});

await app.serve({
  hostname: "localhost",
  port: 8080,
});

// Open http://localhost:8080 or http://localhost:8080/posts in your web browser
```

For a larger example, see `deno-libs/examples/app`

## `deno-libs/css`

File `css/base.css` sets basic styles on some HTML elements.

Copy `base.css` into your project. Then reference them in your HTML page:

```html
<link href="/path/to/base.min.css" rel="stylesheet" />
<link href="/path/to/theme.min.css" rel="stylesheet" />
```

## `deno-libs/ids`

If you do not want to use UUIDv4, you can generate more compact IDs with `readableId()`.

```ts
import { readableId } from "https://gitlab.com/ChristianSiegert/deno-libs/-/raw/master/ids/mod.ts";
console.log(crypto.randomUUID()); // 9e59707-ad9c-40d3-bc21-c16e1fd0ed25
console.log(readableId(20)); // yQsY4gcqXR1LXZrvnqYG
```

## Development

Cache:

```sh
deno task cache
```

Check:

```sh
deno check --unstable **/*.ts
```

Format:

```sh
deno fmt
```

Lint:

```sh
deno lint
```

Test:

```sh
deno task test
deno task test:watch
```

Run example app:

```sh
deno task run:example
deno task run:example:watch
```

### Minifying client-side JavaScript

1. [Download esbuild](https://esbuild.github.io/getting-started/#download-a-build) and place it in
   directory `./tools/`

2. Minify client-side code:

   ```
   ./tools/esbuild --minify --outdir=html/ui/ --out-extension:.js=.min.js --target=es6 html/ui/*.client.ts
   ```

3. Replace value of `script` variable in `html/ui/popup.ts` with content of
   `html/ui/popup.client.min.js`

4. Delete `html/ui/*.min.js`
