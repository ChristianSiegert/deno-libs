import { Component } from "./component.ts";
import { meta } from "./elements.ts";

/**
 * @unstable
 */
export type TwitterSummaryCardOptions = {
  /**
   * A description that concisely summarizes the content as appropriate for
   * presentation within a Tweet. You should not re-use the title as the
   * description or use this field to describe the general services provided by
   * the website. Platform specific behaviors:
   *   - iOS, Android: Not displayed
   *   - Web: Truncated to three lines in timeline and expanded Tweet
   */
  description?: string;

  /**
   * A URL to a unique image representing the content of the page. You should
   * not use a generic image such as your website logo, author photo, or other
   * image that spans multiple pages. Images must be less than 5MB in size. JPG,
   * PNG, WEBP and GIF formats are supported. Only the first frame of an
   * animated GIF will be used. SVG is not supported.
   *
   * Summary Card: Images support an aspect ratio of 1:1 with minimum dimensions
   * of 144x144 or maximum of 4096x4096 pixels. The image will be cropped to a
   * square on all platforms.
   *
   * Summary Card with Large Image: Images support an aspect ratio of 2:1 with
   * minimum dimensions of 300x157 or maximum of 4096x4096 pixels.
   */
  image?: string;

  /**
   * A text description of the image conveying the essential nature of an image
   * to users who are visually impaired. Maximum 420 characters.
   */
  imageAlt?: string;

  /**
   * The Twitter @username the card should be attributed to.
   */
  site?: string;

  /**
   * A concise title for the related content. Platform specific behaviors:
   *   - iOS, Android: Truncated to two lines in timeline and expanded Tweet
   *   - Web: Truncated to one line in timeline and expanded Tweet
   */
  title: string;
};

/**
 * createTwitterSummaryCard creates <meta> tags that describe the page so that
 * Twitter and compatible services have an easier time generating a preview.
 * The returned component is to be placed in the HTML document’s <head> section.
 * <https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/summary>
 * @unstable
 */
export function createTwitterSummaryCard(
  options: TwitterSummaryCardOptions,
): Component {
  return new Component([
    meta({
      name: "twitter:card",
      content: "summary",
    }),
    meta({
      if: options.description,
      name: "twitter:description",
      content: options.description,
    }),
    meta({
      if: options.image,
      name: "twitter:image",
      content: options.image,
    }),
    meta({
      if: options.imageAlt,
      name: "twitter:image:alt",
      content: options.imageAlt,
    }),
    meta({
      if: options.site,
      name: "twitter:site",
      content: options.site,
    }),
    meta({
      name: "twitter:title",
      content: options.title,
    }),
  ]);
}

/**
 * createTwitterSummaryCardWithLargeImage creates <meta> tags that describe the
 * page so that Twitter and compatible services have an easier time generating a
 * preview. The returned component is to be placed in the HTML document’s <head>
 * section.
 * <https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/summary-card-with-large-image>
 * @unstable
 */
export function createTwitterSummaryCardWithLargeImage(
  options: TwitterSummaryCardOptions,
): Component {
  return new Component([
    meta({
      name: "twitter:card",
      content: "summary_large_image",
    }),
    meta({
      if: options.description,
      name: "twitter:description",
      content: options.description,
    }),
    meta({
      if: options.image,
      name: "twitter:image",
      content: options.image,
    }),
    meta({
      if: options.imageAlt,
      name: "twitter:image:alt",
      content: options.imageAlt,
    }),
    meta({
      if: options.site,
      name: "twitter:site",
      content: options.site,
    }),
    meta({
      name: "twitter:title",
      content: options.title,
    }),
  ]);
}
