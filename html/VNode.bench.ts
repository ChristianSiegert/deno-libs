import { Component } from "./component.ts";
import { Attributes, VNode } from "./VNode.ts";

/*
$ deno bench --no-check html/VNode.bench.ts
cpu: Apple M1
runtime: deno 1.32.5 (aarch64-apple-darwin)

file:///<path>/html/VNode.bench.ts
benchmark                     time (avg)             (min … max)       p75       p99      p995
---------------------------------------------------------------- -----------------------------
VNode.toString            14.04 ms/iter(13.92 ms … 15.12 ms) 14.06 ms 15.12 ms 15.12 ms
VNodeAssign.toString      25.85 ms/iter(23.60 ms … 43.18 ms) 25.99 ms 43.18 ms 43.18 ms
VNodeAssignLoop.toString  24.56 ms/iter(23.62 ms … 31.01 ms) 24.78 ms 31.01 ms 31.01 ms
VNodeConcat.toString      24.43 ms/iter(23.53 ms … 30.59 ms) 24.86 ms 30.59 ms 30.59 ms
*/

/** VNode rewritten to use += for string concatenation. */
class VNodeAssign {
  elementName: string;
  attributes: Attributes;
  children: (string | Component | VNodeAssign)[];

  // Elements that cannot have child nodes according to
  // <https://developer.mozilla.org/en-US/docs/Glossary/Void_element>
  static voidElements = new Set<string>([
    "area",
    "base",
    "br",
    "col",
    "embed",
    "hr",
    "img",
    "input",
    "link",
    "meta",
    "param",
    "source",
    "track",
    "wbr",
  ]);

  constructor(
    elementName: string,
    attributes: Attributes = {},
    children: (string | Component | VNodeAssign)[] = [],
  ) {
    if (VNodeAssign.voidElements.has(elementName) && children.length) {
      throw new Error(
        `${VNodeAssign.name}: element "${elementName}" cannot have child nodes`,
      );
    }

    this.elementName = elementName;
    this.attributes = attributes;
    this.children = children;
  }

  #renderAttributes(): string {
    let attributes = "";
    const attributeList = Object.entries(this.attributes).sort();
    for (let i = 0, { length } = attributeList; i < length; i++) {
      const [attributeName, attributeValue] = attributeList[i];
      if (
        attributeValue === false ||
        attributeValue === null ||
        attributeValue === undefined ||
        attributeName === "if" ||
        attributeName === "unsafeHtml"
      ) continue;

      if (attributeValue === true) {
        attributes += ` ${attributeName}`;
        continue;
      }

      if (attributeName === "class" && typeof attributeValue === "object") {
        const classes = Object.entries(attributeValue)
          .filter(([_className, classValue]) => classValue)
          .map(([className]) => className)
          .sort()
          .join(" ");

        attributes += ` class="${this.#sanitizeAttribute(classes)}"`;
        continue;
      }

      attributes += ` ${attributeName}="${this.#sanitizeAttribute(String(attributeValue))}"`;
    }
    return attributes;
  }

  #renderChildren(): string {
    if (
      "unsafeHtml" in this.attributes &&
      typeof this.attributes.unsafeHtml === "string"
    ) return this.attributes.unsafeHtml;

    let children = "";
    for (let i = 0, { length } = this.children; i < length; i++) {
      const child = this.children[i];
      children += typeof child === "string" &&
          this.elementName !== "script" &&
          this.elementName !== "style"
        ? this.#sanitizeTextContent(child)
        : child.toString();
    }
    return children;
  }

  #sanitizeAttribute(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll('"', "&quot;");
  }

  #sanitizeTextContent(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;");
  }

  toString(): string {
    if ("if" in this.attributes && !this.attributes.if) return "";

    return VNodeAssign.voidElements.has(this.elementName)
      ? `<${this.elementName}${this.#renderAttributes()}>`
      : `<${this.elementName}${this.#renderAttributes()}>${this.#renderChildren()}</${this.elementName}>`;
  }
}

/** VNode rewritten to use += for string concatenation in for...of loop */
class VNodeAssignLoop {
  elementName: string;
  attributes: Attributes;
  children: (string | Component | VNodeAssignLoop)[];

  // Elements that cannot have child nodes according to
  // <https://developer.mozilla.org/en-US/docs/Glossary/Void_element>
  static voidElements = new Set<string>([
    "area",
    "base",
    "br",
    "col",
    "embed",
    "hr",
    "img",
    "input",
    "link",
    "meta",
    "param",
    "source",
    "track",
    "wbr",
  ]);

  constructor(
    elementName: string,
    attributes: Attributes = {},
    children: (string | Component | VNodeAssignLoop)[] = [],
  ) {
    if (
      VNodeAssignLoop.voidElements.has(elementName) && children.length
    ) {
      throw new Error(
        `${VNodeAssignLoop.name}: element "${elementName}" cannot have child nodes`,
      );
    }

    this.elementName = elementName;
    this.attributes = attributes;
    this.children = children;
  }

  #renderAttributes(): string {
    let attributes = "";
    const attributeList = Object.entries(this.attributes).sort();
    for (const [attributeName, attributeValue] of attributeList) {
      if (
        attributeValue === false ||
        attributeValue === null ||
        attributeValue === undefined ||
        attributeName === "if" ||
        attributeName === "unsafeHtml"
      ) continue;

      if (attributeValue === true) {
        attributes += ` ${attributeName}`;
        continue;
      }

      if (attributeName === "class" && typeof attributeValue === "object") {
        const classes = Object.entries(attributeValue)
          .filter(([_className, classValue]) => classValue)
          .map(([className]) => className)
          .sort()
          .join(" ");

        attributes += ` class="${this.#sanitizeAttribute(classes)}"`;
        continue;
      }

      attributes += ` ${attributeName}="${this.#sanitizeAttribute(String(attributeValue))}"`;
    }
    return attributes;
  }

  #renderChildren(): string {
    if (
      "unsafeHtml" in this.attributes &&
      typeof this.attributes.unsafeHtml === "string"
    ) return this.attributes.unsafeHtml;

    let children = "";
    for (let i = 0, { length } = this.children; i < length; i++) {
      const child = this.children[i];
      children += typeof child === "string" &&
          this.elementName !== "script" &&
          this.elementName !== "style"
        ? this.#sanitizeTextContent(child)
        : child.toString();
    }
    return children;
  }

  #sanitizeAttribute(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll('"', "&quot;");
  }

  #sanitizeTextContent(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;");
  }

  toString(): string {
    if ("if" in this.attributes && !this.attributes.if) return "";

    return VNodeAssignLoop.voidElements.has(this.elementName)
      ? `<${this.elementName}${this.#renderAttributes()}>`
      : `<${this.elementName}${this.#renderAttributes()}>${this.#renderChildren()}</${this.elementName}>`;
  }
}

/** VNode rewritten to use Array.prototype.concat() for string concatenation. */
class VNodeConcat {
  elementName: string;
  attributes: Attributes;
  children: (string | Component | VNodeConcat)[];

  // Elements that cannot have child nodes according to
  // <https://developer.mozilla.org/en-US/docs/Glossary/Void_element>
  static voidElements = new Set<string>([
    "area",
    "base",
    "br",
    "col",
    "embed",
    "hr",
    "img",
    "input",
    "link",
    "meta",
    "param",
    "source",
    "track",
    "wbr",
  ]);

  constructor(
    elementName: string,
    attributes: Attributes = {},
    children: (string | Component | VNodeConcat)[] = [],
  ) {
    if (VNodeConcat.voidElements.has(elementName) && children.length) {
      throw new Error(
        `${VNodeConcat.name}: element "${elementName}" cannot have child nodes`,
      );
    }

    this.elementName = elementName;
    this.attributes = attributes;
    this.children = children;
  }

  #renderAttributes(): string {
    let attributes = "";
    const attributeList = Object.entries(this.attributes).sort();
    for (let i = 0, { length } = attributeList; i < length; i++) {
      const [attributeName, attributeValue] = attributeList[i];
      if (
        attributeValue === false ||
        attributeValue === null ||
        attributeValue === undefined ||
        attributeName === "if" ||
        attributeName === "unsafeHtml"
      ) continue;

      if (attributeValue === true) {
        attributes = attributes.concat(` ${attributeName}`);
        continue;
      }

      if (attributeName === "class" && typeof attributeValue === "object") {
        const classes = Object.entries(attributeValue)
          .filter(([_className, classValue]) => classValue)
          .map(([className]) => className)
          .sort()
          .join(" ");

        attributes = attributes.concat(
          ` class="${this.#sanitizeAttribute(classes)}"`,
        );
        continue;
      }

      attributes = attributes.concat(
        ` ${attributeName}="${this.#sanitizeAttribute(String(attributeValue))}"`,
      );
    }
    return attributes;
  }

  #renderChildren(): string {
    if (
      "unsafeHtml" in this.attributes &&
      typeof this.attributes.unsafeHtml === "string"
    ) return this.attributes.unsafeHtml;

    let children = "";
    for (let i = 0, { length } = this.children; i < length; i++) {
      const child = this.children[i];
      children = children.concat(
        typeof child === "string" &&
          this.elementName !== "script" &&
          this.elementName !== "style"
          ? this.#sanitizeTextContent(child)
          : child.toString(),
      );
    }
    return children;
  }

  #sanitizeAttribute(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll('"', "&quot;");
  }

  #sanitizeTextContent(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;");
  }

  toString(): string {
    if ("if" in this.attributes && !this.attributes.if) return "";

    let parts = `<${this.elementName}${this.#renderAttributes()}>`;

    if (!VNodeConcat.voidElements.has(this.elementName)) {
      parts = parts.concat(this.#renderChildren(), "</", this.elementName, ">");
    }

    return parts;
  }
}

const childCount = 1000;
const grandchildCount = 10;
const attributeCount = 5;

const attributes = Object.fromEntries(
  Array.from({ length: attributeCount }).map((_, i) => [`a${i}`, `v${i}`]),
);

const vnodes = [VNode, VNodeAssign, VNodeAssignLoop, VNodeConcat].map((Class) =>
  Array.from({ length: childCount }).map((_, i) =>
    new Class("div", attributes, [
      `Child ${i}`,
      ...Array.from({ length: grandchildCount }).map((_, i) =>
        new Class("div", attributes, [`Grandchild ${i}`])
        // deno-lint-ignore no-explicit-any
      ) as any,
    ])
  )
);

Deno.bench("VNode.toString", { baseline: true }, () => {
  vnodes[0].toString();
});

Deno.bench("VNodeAssign.toString", () => {
  vnodes[1].toString();
});

Deno.bench("VNodeAssignLoop.toString", () => {
  vnodes[2].toString();
});

Deno.bench("VNodeConcat.toString", () => {
  vnodes[3].toString();
});
