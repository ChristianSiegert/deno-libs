import { Component } from "./component.ts";

/**
 * If attribute `class` is a record, the key of each property is added to the class list if the property value is `true`.
 *
 * If attribute `if` is set and its value is falsy, the element is not rendered.
 *
 * If attribute `unsafeHtml` is set, the provided string is used as child node without escaping.
 */
export type Attributes =
  | { class: Record<string, boolean> }
  | { if: unknown }
  | { unsafeHtml: string }
  | { [key: string]: string | number | boolean | null | undefined };

export type VNodeChild = string | null | undefined | Component | VNode;

export type ElementName =
  | "a"
  | "abbr"
  | "address"
  | "area"
  | "article"
  | "aside"
  | "audio"
  | "b"
  | "base"
  | "bdi"
  | "bdo"
  | "blockquote"
  | "body"
  | "br"
  | "button"
  | "canvas"
  | "caption"
  | "cite"
  | "code"
  | "col"
  | "colgroup"
  | "data"
  | "datalist"
  | "dd"
  | "del"
  | "details"
  | "dfn"
  | "dialog"
  | "div"
  | "dl"
  | "dt"
  | "em"
  | "embed"
  | "fieldset"
  | "figcaption"
  | "figure"
  | "footer"
  | "form"
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "head"
  | "header"
  | "hr"
  | "html"
  | "i"
  | "iframe"
  | "img"
  | "input"
  | "ins"
  | "kbd"
  | "label"
  | "legend"
  | "li"
  | "link"
  | "main"
  | "map"
  | "mark"
  | "math"
  | "menu"
  | "meta"
  | "meter"
  | "nav"
  | "noscript"
  | "object"
  | "ol"
  | "optgroup"
  | "option"
  | "output"
  | "p"
  | "picture"
  | "portal"
  | "pre"
  | "progress"
  | "q"
  | "rp"
  | "rt"
  | "ruby"
  | "s"
  | "samp"
  | "script"
  | "section"
  | "select"
  | "slot"
  | "small"
  | "source"
  | "span"
  | "strong"
  | "style"
  | "sub"
  | "summary"
  | "sup"
  | "svg"
  | "table"
  | "tbody"
  | "td"
  | "template"
  | "textarea"
  | "tfoot"
  | "th"
  | "thead"
  | "time"
  | "title"
  | "tr"
  | "track"
  | "u"
  | "ul"
  | "var"
  | "video"
  | "wbr";

export class VNode {
  elementName: string;
  attributes: Attributes;
  children: VNodeChild[];

  /** Elements that cannot have child nodes according to
   * [MDN](https://developer.mozilla.org/en-US/docs/Glossary/Void_element). */
  static voidElements = new Set<string>([
    "area",
    "base",
    "br",
    "col",
    "embed",
    "hr",
    "img",
    "input",
    "link",
    "meta",
    "param",
    "source",
    "track",
    "wbr",
  ]);

  constructor(
    elementName: string,
    attributes: Attributes = {},
    children: VNodeChild[] = [],
  ) {
    if (VNode.voidElements.has(elementName) && children.length) {
      throw new Error(
        `${VNode.name}: element "${elementName}" cannot have child nodes`,
      );
    }

    this.elementName = elementName;
    this.attributes = attributes;
    this.children = children;
  }

  #renderAttributes(): string {
    return Object.entries(this.attributes)
      .map(([attributeName, attributeValue]): string => {
        if (
          attributeValue === false ||
          attributeValue === null ||
          attributeValue === undefined ||
          attributeName === "if" ||
          attributeName === "unsafeHtml"
        ) return "";

        if (attributeValue === true) {
          return ` ${attributeName}`;
        }

        if (attributeName === "class" && typeof attributeValue === "object") {
          const classes = Object.entries(attributeValue)
            .filter(([_className, classValue]) => classValue)
            .map(([className]) => className)
            .sort()
            .join(" ");

          return ` class="${this.#sanitizeAttribute(classes)}"`;
        }

        return ` ${attributeName}="${this.#sanitizeAttribute(String(attributeValue))}"`;
      })
      .sort()
      .join("");
  }

  #renderChildren(): string {
    if (
      "unsafeHtml" in this.attributes &&
      typeof this.attributes.unsafeHtml === "string"
    ) return this.attributes.unsafeHtml;

    let children = "";
    for (let i = 0, { length } = this.children; i < length; i++) {
      const child = this.children[i];
      if (!child) continue;
      children += typeof child === "string" &&
          this.elementName !== "script" &&
          this.elementName !== "style"
        ? this.#sanitizeTextContent(child)
        : child.toString();
    }
    return children;
  }

  #sanitizeAttribute(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll('"', "&quot;");
  }

  #sanitizeTextContent(value: string): string {
    return value
      .replaceAll("&", "&amp;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;");
  }

  toString(): string {
    if ("if" in this.attributes && !this.attributes.if) return "";
    return VNode.voidElements.has(this.elementName)
      ? `<${this.elementName}${this.#renderAttributes()}>`
      : `<${this.elementName}${this.#renderAttributes()}>${this.#renderChildren()}</${this.elementName}>`;
  }
}

export function h(
  elementName: ElementName,
): VNode;

export function h(
  elementName: ElementName,
  attributes: Attributes,
): VNode;

export function h(
  elementName: ElementName,
  children: VNodeChild | VNodeChild[],
): VNode;

export function h(
  elementName: ElementName,
  attributes: Attributes,
  children: VNodeChild | VNodeChild[],
): VNode;

export function h(
  elementName: ElementName,
  arg2?: Attributes | VNodeChild | VNodeChild[],
  arg3?: VNodeChild | VNodeChild[],
): VNode;

export function h(
  elementName: ElementName,
  arg2?: Attributes | VNodeChild | VNodeChild[],
  arg3?: VNodeChild | VNodeChild[],
): VNode {
  const attributes: Attributes | undefined = isAttributesObject(arg2) ? arg2 : undefined;
  const children: VNodeChild[] = isAttributesObject(arg2)
    ? (Array.isArray(arg3) ? arg3 : (arg3 ? [arg3] : []))
    : (Array.isArray(arg2) ? arg2 : (arg2 ? [arg2] : []));
  return new VNode(elementName, attributes, children);
}

function isAttributesObject(value: unknown): value is Attributes {
  return typeof value === "object" &&
    value !== null &&
    !Array.isArray(value) &&
    !(value instanceof Component) &&
    !(value instanceof VNode);
}
