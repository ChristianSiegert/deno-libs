import { VNode } from "./VNode.ts";

export class Component {
  content: (Component | VNode | string | null | undefined)[];

  scripts: string[] = [];

  styles: string[] = [];

  constructor(
    content: (Component | VNode | string | null | undefined)[] = [],
    options?: {
      script?: string;
      scripts?: string[];
      style?: string;
      styles?: string[];
    },
  ) {
    this.content = content;

    if (options?.script) {
      this.scripts.push(options.script);
    }
    if (options?.scripts?.length) {
      this.scripts.push(...options.scripts);
    }
    if (options?.style) {
      this.styles.push(options.style);
    }
    if (options?.styles?.length) {
      this.styles.push(...options.styles);
    }
  }

  /** Returns the scripts of this component and its child components. */
  getScripts(): string[] {
    const scripts = new Set<string>();

    getComponents(this).forEach((component) => {
      component.scripts.forEach((script) => scripts.add(script));
    });

    return Array.from(scripts);
  }

  /** Returns the styles of this component and its child components. */
  getStyles(): string[] {
    const styles = new Set<string>();

    getComponents(this).forEach((component) => {
      component.styles.forEach((style) => styles.add(style));
    });

    return Array.from(styles);
  }

  /** Returns the component’s content as HTML. */
  toString(): string {
    let html = "";
    for (const child of this.content) {
      if (!child) continue;
      if (child instanceof VNode || child instanceof Component) {
        html += child.toString();
      } else if (typeof child === "string") {
        html += sanitizeTextContent(child);
      }
    }
    return html;
  }
}

function getComponents(
  node: Component | VNode | string | null | undefined,
): Component[] {
  if (node instanceof Component) {
    return [node, ...node.content.flatMap(getComponents)];
  } else if (node instanceof VNode) {
    return node.children.flatMap(getComponents);
  } else {
    return [];
  }
}

function sanitizeTextContent(value: string) {
  return value
    .replaceAll("&", "&amp;")
    .replaceAll("<", "&lt;")
    .replaceAll(">", "&gt;");
}
