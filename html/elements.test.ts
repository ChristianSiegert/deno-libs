import { assertEquals } from "../deps.ts";
import {
  a,
  abbr,
  address,
  area,
  article,
  aside,
  audio,
  b,
  base,
  bdi,
  bdo,
  blockquote,
  body,
  br,
  button,
  canvas,
  caption,
  cite,
  code,
  col,
  colgroup,
  data,
  datalist,
  dd,
  del,
  details,
  dfn,
  dialog,
  div,
  dl,
  dt,
  em,
  embed,
  fieldset,
  figcaption,
  figure,
  footer,
  form,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  head,
  header,
  hr,
  html,
  i,
  iframe,
  img,
  input,
  ins,
  kbd,
  label,
  legend,
  li,
  link,
  main,
  map,
  mark,
  math,
  menu,
  meta,
  meter,
  nav,
  noscript,
  object,
  ol,
  optgroup,
  option,
  output,
  p,
  picture,
  portal,
  pre,
  progress,
  q,
  rp,
  rt,
  ruby,
  s,
  samp,
  script,
  section,
  select,
  slot,
  small,
  source,
  span,
  strong,
  style,
  sub,
  summary,
  sup,
  svg,
  table,
  tbody,
  td,
  template,
  textarea,
  tfoot,
  th,
  thead,
  time,
  title,
  tr,
  track,
  u,
  ul,
  video,
  wbr,
} from "./elements.ts";

Deno.test("HTML elements", () => {
  assertEquals(a().toString(), "<a></a>");
  assertEquals(abbr().toString(), "<abbr></abbr>");
  assertEquals(address().toString(), "<address></address>");
  assertEquals(area().toString(), "<area>");
  assertEquals(article().toString(), "<article></article>");
  assertEquals(aside().toString(), "<aside></aside>");
  assertEquals(audio().toString(), "<audio></audio>");
  assertEquals(b().toString(), "<b></b>");
  assertEquals(base().toString(), "<base>");
  assertEquals(bdi().toString(), "<bdi></bdi>");
  assertEquals(bdo().toString(), "<bdo></bdo>");
  assertEquals(blockquote().toString(), "<blockquote></blockquote>");
  assertEquals(body().toString(), "<body></body>");
  assertEquals(br().toString(), "<br>");
  assertEquals(button().toString(), "<button></button>");
  assertEquals(canvas().toString(), "<canvas></canvas>");
  assertEquals(caption().toString(), "<caption></caption>");
  assertEquals(cite().toString(), "<cite></cite>");
  assertEquals(code().toString(), "<code></code>");
  assertEquals(col().toString(), "<col>");
  assertEquals(colgroup().toString(), "<colgroup></colgroup>");
  assertEquals(data().toString(), "<data></data>");
  assertEquals(datalist().toString(), "<datalist></datalist>");
  assertEquals(dd().toString(), "<dd></dd>");
  assertEquals(del().toString(), "<del></del>");
  assertEquals(details().toString(), "<details></details>");
  assertEquals(dfn().toString(), "<dfn></dfn>");
  assertEquals(dialog().toString(), "<dialog></dialog>");
  assertEquals(div().toString(), "<div></div>");
  assertEquals(dl().toString(), "<dl></dl>");
  assertEquals(dt().toString(), "<dt></dt>");
  assertEquals(em().toString(), "<em></em>");
  assertEquals(embed().toString(), "<embed>");
  assertEquals(fieldset().toString(), "<fieldset></fieldset>");
  assertEquals(figcaption().toString(), "<figcaption></figcaption>");
  assertEquals(figure().toString(), "<figure></figure>");
  assertEquals(footer().toString(), "<footer></footer>");
  assertEquals(form().toString(), "<form></form>");
  assertEquals(h1().toString(), "<h1></h1>");
  assertEquals(h2().toString(), "<h2></h2>");
  assertEquals(h3().toString(), "<h3></h3>");
  assertEquals(h4().toString(), "<h4></h4>");
  assertEquals(h5().toString(), "<h5></h5>");
  assertEquals(h6().toString(), "<h6></h6>");
  assertEquals(head().toString(), "<head></head>");
  assertEquals(header().toString(), "<header></header>");
  assertEquals(hr().toString(), "<hr>");
  assertEquals(html().toString(), "<html></html>");
  assertEquals(i().toString(), "<i></i>");
  assertEquals(iframe().toString(), "<iframe></iframe>");
  assertEquals(img().toString(), "<img>");
  assertEquals(input().toString(), "<input>");
  assertEquals(ins().toString(), "<ins></ins>");
  assertEquals(kbd().toString(), "<kbd></kbd>");
  assertEquals(label().toString(), "<label></label>");
  assertEquals(legend().toString(), "<legend></legend>");
  assertEquals(li().toString(), "<li></li>");
  assertEquals(link().toString(), "<link>");
  assertEquals(main().toString(), "<main></main>");
  assertEquals(map().toString(), "<map></map>");
  assertEquals(mark().toString(), "<mark></mark>");
  assertEquals(math().toString(), "<math></math>");
  assertEquals(menu().toString(), "<menu></menu>");
  assertEquals(meta().toString(), "<meta>");
  assertEquals(meter().toString(), "<meter></meter>");
  assertEquals(nav().toString(), "<nav></nav>");
  assertEquals(noscript().toString(), "<noscript></noscript>");
  assertEquals(object().toString(), "<object></object>");
  assertEquals(ol().toString(), "<ol></ol>");
  assertEquals(optgroup().toString(), "<optgroup></optgroup>");
  assertEquals(option().toString(), "<option></option>");
  assertEquals(output().toString(), "<output></output>");
  assertEquals(p().toString(), "<p></p>");
  assertEquals(picture().toString(), "<picture></picture>");
  assertEquals(portal().toString(), "<portal></portal>");
  assertEquals(pre().toString(), "<pre></pre>");
  assertEquals(progress().toString(), "<progress></progress>");
  assertEquals(q().toString(), "<q></q>");
  assertEquals(rp().toString(), "<rp></rp>");
  assertEquals(rt().toString(), "<rt></rt>");
  assertEquals(ruby().toString(), "<ruby></ruby>");
  assertEquals(s().toString(), "<s></s>");
  assertEquals(samp().toString(), "<samp></samp>");
  assertEquals(script().toString(), "<script></script>");
  assertEquals(section().toString(), "<section></section>");
  assertEquals(select().toString(), "<select></select>");
  assertEquals(slot().toString(), "<slot></slot>");
  assertEquals(small().toString(), "<small></small>");
  assertEquals(source().toString(), "<source>");
  assertEquals(span().toString(), "<span></span>");
  assertEquals(strong().toString(), "<strong></strong>");
  assertEquals(style().toString(), "<style></style>");
  assertEquals(sub().toString(), "<sub></sub>");
  assertEquals(summary().toString(), "<summary></summary>");
  assertEquals(sup().toString(), "<sup></sup>");
  assertEquals(svg().toString(), "<svg></svg>");
  assertEquals(table().toString(), "<table></table>");
  assertEquals(tbody().toString(), "<tbody></tbody>");
  assertEquals(td().toString(), "<td></td>");
  assertEquals(template().toString(), "<template></template>");
  assertEquals(textarea().toString(), "<textarea></textarea>");
  assertEquals(tfoot().toString(), "<tfoot></tfoot>");
  assertEquals(th().toString(), "<th></th>");
  assertEquals(thead().toString(), "<thead></thead>");
  assertEquals(time().toString(), "<time></time>");
  assertEquals(title().toString(), "<title></title>");
  assertEquals(tr().toString(), "<tr></tr>");
  assertEquals(track().toString(), "<track>");
  assertEquals(u().toString(), "<u></u>");
  assertEquals(ul().toString(), "<ul></ul>");
  assertEquals(video().toString(), "<video></video>");
  assertEquals(wbr().toString(), "<wbr>");
});
