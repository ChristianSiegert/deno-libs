import { assertEquals } from "../deps.ts";
import { Component } from "./component.ts";
import { VNode } from "./VNode.ts";

Deno.test(Component.name, () => {
  let component = new Component([]);
  assertEquals(component.toString(), "");
  assertEquals(component.getScripts(), []);
  assertEquals(component.getStyles(), []);

  component = new Component([
    new Component([
      new Component([
        new Component([
          new VNode("x"),
        ], { script: "script-4", style: "style-4" }),
      ], { script: "script-3", style: "style-3" }),
    ], { script: "script-2", style: "style-2" }),
    new VNode("a", {}, [
      new VNode("b", {}, [
        new Component([
          new VNode("c"),
        ], { script: "script-5", style: "style-5" }),
        new Component([
          new VNode("c"),
        ], { script: "script-5", style: "style-5" }),
      ]),
    ]),
  ], { script: "script-1", style: "style-1" });

  component.content.push(new VNode("u", {}, ["amended"]));
  component.scripts[0] += " script amended";
  component.styles[0] += " style amended";

  assertEquals(
    component.toString(),
    "<x></x><a><b><c></c><c></c></b></a><u>amended</u>",
  );
  assertEquals(component.getScripts(), [
    "script-1 script amended",
    "script-2",
    "script-3",
    "script-4",
    "script-5",
  ]);
  assertEquals(component.getStyles(), [
    "style-1 style amended",
    "style-2",
    "style-3",
    "style-4",
    "style-5",
  ]);

  // Does not stringify null, undefined
  assertEquals(
    new Component(["", null, undefined, "1"]).toString(),
    "1",
  );

  // HTML-escapes strings
  assertEquals(
    new Component([
      "<a>1 & 2</a>",
      new Component([
        "<b>3 & 4</b>",
        new Component([
          "<c>5 & 6</c>",
        ]),
      ]),
    ]).toString(),
    "&lt;a&gt;1 &amp; 2&lt;/a&gt;&lt;b&gt;3 &amp; 4&lt;/b&gt;&lt;c&gt;5 &amp; 6&lt;/c&gt;",
  );
});
