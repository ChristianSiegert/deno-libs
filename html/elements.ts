import { type Attributes as VNodeAttributes, h, type VNode, type VNodeChild } from "./VNode.ts";

export type HtmlAttributeAutocomplete =
  | "additional-name"
  | "address-level1"
  | "address-level2"
  | "address-level3"
  | "address-level4"
  | "address-line1"
  | "address-line2"
  | "address-line3"
  | "bday-day"
  | "bday-month"
  | "bday-year"
  | "bday"
  | "billing"
  | "cc-additional-name"
  | "cc-csc"
  | "cc-exp-month"
  | "cc-exp-year"
  | "cc-exp"
  | "cc-family-name"
  | "cc-given-name"
  | "cc-name"
  | "cc-number"
  | "cc-type"
  | "country-name"
  | "country"
  | "current-password"
  | "email"
  | "family-name"
  | "given-name"
  | "honorific-prefix"
  | "honorific-suffix"
  | "impp"
  | "language"
  | "name"
  | "new-password"
  | "nickname"
  | "off"
  | "on"
  | "one-time-code"
  | "organization-title"
  | "organization"
  | "photo"
  | "postal-code"
  | "sex"
  | "shipping"
  | "street-address"
  | "tel-area-code"
  | "tel-country-code"
  | "tel-extension"
  | "tel-local-prefix"
  | "tel-local-suffix"
  | "tel-local"
  | "tel-national"
  | "tel"
  | "transaction-amount"
  | "transaction-currency"
  | "url"
  | "username"
  | "webauthn";

export type HtmlAttributeFormEnctype =
  | "application/x-www-form-urlencoded"
  | "multipart/form-data"
  | "text/plain";

export type HtmlAttributeReferrerPolicy =
  | "no-referrer"
  | "no-referrer-when-downgrade"
  | "origin"
  | "origin-when-cross-origin"
  | "same-origin"
  | "strict-origin"
  | "strict-origin-when-cross-origin"
  | "unsafe-url";

export type HtmlAttributeTarget = "_blank" | "_parent" | "_self" | "_top";

export type HtmlGlobalAttributes = {
  accesskey?: string;
  autocapitalize?: "characters" | "none" | "off" | "on" | "sentences" | "words";
  autofocus?: boolean;
  class?: string;
  contenteditable?: "false" | "true";
  dir?: "auto" | "ltr" | "rtl";
  draggable?: "false" | "true";
  enterkeyhint?: string;
  hidden?: "hidden" | "until-found";
  id?: string;
  inert?: boolean;
  inputmode?:
    | "decimal"
    | "email"
    | "none"
    | "numeric"
    | "search"
    | "tel"
    | "text"
    | "url";
  is?: string;
  itemid?: string;
  itemprop?: string;
  itemref?: string;
  itemscope?: string;
  itemtype?: string;
  lang?: string;
  nonce?: string;
  part?: string;
  popover?: string;
  role?: string;
  slot?: string;
  spellcheck?: string;
  style?: string;
  tabindex?: number;
  title?: string;
  translate?: "no" | "yes";
  virtualkeyboardpolicy?: "auto" | "manual";
};

export type HtmlAnchorAttributes = HtmlGlobalAttributes & {
  download?: boolean | string;
  href?: string;
  hreflang?: string;
  ping?: string;
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  rel?: string;
  target?: HtmlAttributeTarget;
  type?: string;
};

export type HtmlAreaAttributes = HtmlGlobalAttributes & {
  alt?: string;
  coords?: string;
  download?: boolean | string;
  href?: string;
  ping?: string;
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  rel?: string;
  shape?: "circle" | "default" | "poly" | "rect";
  target?: HtmlAttributeTarget;
};

export type HtmlAudioAttributes = HtmlGlobalAttributes & {
  autoplay?: boolean;
  controls?: boolean;
  crossorigin?: "anonymous" | "use-credentials";
  loop?: boolean;
  muted?: boolean;
  preload?: "auto" | "metadata" | "none";
  src?: string;
};

export type HtmlBaseAttributes = HtmlGlobalAttributes & {
  href?: string;
  target?: HtmlAttributeTarget;
};

export type HtmlBlockquoteAttributes = HtmlGlobalAttributes & {
  cite?: string;
};

export type HtmlButtonAttributes = HtmlGlobalAttributes & {
  autofocus?: boolean;
  disabled?: boolean;
  form?: string;
  formaction?: string;
  formenctype?: HtmlAttributeFormEnctype;
  formmethod?: "dialog" | "get" | "post";
  formnovalidate?: boolean;
  formtarget?: HtmlAttributeTarget;
  name?: string;
  popovertarget?: string;
  popovertargetaction?: "hide" | "show" | "toggle";
  type?: "button" | "reset" | "submit";
  value?: string;
};

export type HtmlColAttributes = HtmlGlobalAttributes & {
  span?: number;
};

export type HtmlColGroupAttributes = HtmlGlobalAttributes & {
  span?: number;
};

export type HtmlDataAttributes = HtmlGlobalAttributes & {
  value?: string;
};

export type HtmlDelAttributes = HtmlGlobalAttributes & {
  cite?: string;
  datetime?: string;
};

export type HtmlDetailsAttributes = HtmlGlobalAttributes & {
  open?: boolean;
};

export type HtmlDialogAttributes = HtmlGlobalAttributes & {
  open?: boolean;
};

export type HtmlEmbedAttributes = HtmlGlobalAttributes & {
  height?: number;
  src?: string;
  type?: string;
  width?: number;
};

export type HtmlFieldsetAttributes = HtmlGlobalAttributes & {
  disabled?: boolean;
  form?: string;
  name?: string;
};

export type HtmlFormAttributes = HtmlGlobalAttributes & {
  "accept-charset"?: string;
  action?: string;
  autocomplete?: HtmlAttributeAutocomplete;
  enctype?: HtmlAttributeFormEnctype;
  method?: "dialog" | "get" | "post";
  name?: string;
  novalidate?: boolean;
  rel?: string;
  target?: HtmlAttributeTarget;
};

export type HtmlHtmlAttributes = HtmlGlobalAttributes & {
  xmlns?: string;
};

export type HtmlIframeAttributes = HtmlGlobalAttributes & {
  allow?: string;
  height?: number;
  loading?: "eager" | "lazy";
  name?: string;
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  sandbox?: string;
  src?: string;
  srcdoc?: string;
  width?: number;
};

export type HtmlImgAttributes = HtmlGlobalAttributes & {
  alt?: string;
  crossorigin?: "anonymous" | "use-credentials";
  decoding?: "async" | "auto" | "sync";
  elementtiming?: string;
  fetchpriority?: "auto" | "high" | "low";
  height?: number;
  ismap?: boolean;
  loading?: "eager" | "lazy";
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  sizes?: string;
  src?: string;
  srcset?: string;
  width?: number;
  usemap?: string;
};

export type HtmlInputAttributes = HtmlGlobalAttributes & {
  accept?: string;
  alt?: string;
  autocomplete?: HtmlAttributeAutocomplete;
  autocorrect?: "off" | "on";
  autofocus?: boolean;
  capture?: "environment" | "user";
  checked?: boolean;
  dirname?: string;
  disabled?: boolean;
  form?: string;
  formaction?: string;
  formenctype?: HtmlAttributeFormEnctype;
  formmethod?: "dialog" | "get" | "post";
  formnovalidate?: boolean;
  formtarget?: HtmlAttributeTarget;
  height?: number;
  id?: string;
  inputmode?:
    | "decimal"
    | "email"
    | "none"
    | "numeric"
    | "search"
    | "tel"
    | "text"
    | "url";
  list?: string;
  max?: number;
  maxlength?: number;
  min?: number;
  minlength?: number;
  multiple?: boolean;
  name?: string;
  pattern?: string;
  placeholder?: string;
  popovertarget?: string;
  popovertargetaction?: "hide" | "show" | "toggle";
  readonly?: boolean;
  required?: boolean;
  size?: number;
  src?: string;
  step?: number;
  type?:
    | "button"
    | "checkbox"
    | "color"
    | "date"
    | "datetime-local"
    | "email"
    | "file"
    | "hidden"
    | "image"
    | "month"
    | "number"
    | "password"
    | "radio"
    | "range"
    | "reset"
    | "search"
    | "submit"
    | "tel"
    | "text"
    | "time"
    | "url"
    | "week";
  value?: string;
  width?: number;
};

export type HtmlInsAttributes = HtmlGlobalAttributes & {
  cite?: string;
  datetime?: string;
};

export type HtmlLabelAttributes = HtmlGlobalAttributes & {
  for?: string;
};
export type HtmlLiAttributes = HtmlGlobalAttributes & {
  value?: number;
};

export type HtmlLinkAttributes = HtmlGlobalAttributes & {
  as?: string;
  crossorigin?: "anonymous" | "use-credentials";
  href?: string;
  hreflang?: string;
  imagesizes?: string;
  imagesrcset?: string;
  integrity?: string;
  media?: string;
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  rel?: string;
  sizes?: string;
  type?: string;
};

export type HtmlMapAttributes = HtmlGlobalAttributes & {
  name?: string;
};

export type HtmlMathAttributes = MathmlGlobalAttributes | {
  display?: "block" | "inline";
};

export type HtmlMetaAttributes = HtmlGlobalAttributes & {
  charset?: "utf-8" | string;
  content?: string;
  "http-equiv"?:
    | "content-security-policy"
    | "content-type"
    | "default-style"
    | "refresh"
    | "x-ua-compatible";
  name?: string;
};

export type HtmlMeterAttributes = HtmlGlobalAttributes & {
  form?: string;
  high?: number;
  low?: number;
  max?: number;
  min?: number;
  optimum?: number;
  value?: number;
};

export type HtmlObjectAttributes = HtmlGlobalAttributes & {
  data?: string;
  form?: string;
  height?: number;
  name?: string;
  type?: string;
  usemap?: string;
  width?: number;
};

export type HtmlOlAttributes = HtmlGlobalAttributes & {
  reversed?: boolean;
  start?: number;
  type?: "a" | "A" | "i" | "I" | "1";
};

export type HtmlOptGroupAttributes = HtmlGlobalAttributes & {
  disabled?: boolean;
  label?: string;
};

export type HtmlOptionAttributes = HtmlGlobalAttributes & {
  disabled?: boolean;
  label?: string;
  selected?: boolean;
  value?: string;
};

export type HtmlOutputAttributes = HtmlGlobalAttributes & {
  for?: string;
  form?: string;
  name?: string;
};

export type HtmlPortalAttributes = HtmlGlobalAttributes & {
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  src?: string;
};

export type HtmlProgressAttributes = HtmlGlobalAttributes & {
  max?: number;
  value?: number;
};

export type HtmlQAttributes = HtmlGlobalAttributes & {
  cite?: string;
};

export type HtmlScriptAttributes = HtmlGlobalAttributes & {
  async?: boolean;
  crossorigin?: "anonymous" | "use-credentials";
  defer?: boolean;
  integrity?: string;
  nomodule?: boolean;
  referrerpolicy?: HtmlAttributeReferrerPolicy;
  src?: string;
  type?: "importmap" | "module";
};

export type HtmlSelectAttributes = HtmlGlobalAttributes & {
  autocomplete?: HtmlAttributeAutocomplete;
  autofocus?: boolean;
  disabled?: boolean;
  form?: string;
  multiple?: boolean;
  name?: string;
  required?: boolean;
  size?: string;
};

export type HtmlSourceAttributes = HtmlGlobalAttributes & {
  height?: number;
  media?: string;
  sizes?: string;
  src?: string;
  srcset?: string;
  type?: string;
  width?: number;
};

export type HtmlStyleAttributes = HtmlGlobalAttributes & {
  media?: string;
};

export type HtmlTdAttributes = HtmlGlobalAttributes & {
  colspan?: number;
  headers?: string;
  rowspan?: number;
};

export type HtmlTextareaAttributes = HtmlGlobalAttributes & {
  autocomplete?: "off" | "on";
  autocorrect?: "off" | "on";
  cols?: number;
  dirname?: string;
  disabled?: boolean;
  form?: string;
  maxlength?: number;
  minlength?: number;
  name?: string;
  placeholder?: string;
  readonly?: boolean;
  required?: boolean;
  rows?: number;
  spellcheck?: "default" | "false" | "true";
  wrap?: "hard" | "off" | "soft";
};

export type HtmlThAttributes = HtmlGlobalAttributes & {
  abbr?: string;
  colspan?: number;
  headers?: string;
  rowspan?: number;
  scope?: "col" | "colgroup" | "row" | "rowgrup";
};

export type HtmlTimeAttributes = HtmlGlobalAttributes & {
  datetime?: string;
};

export type HtmlTrackAttributes = HtmlGlobalAttributes & {
  default?: boolean;
  kind?: "captions" | "chapters" | "descriptions" | "metadata" | "subtitles";
  label?: string;
  src?: string;
  srclang?: string;
};

export type HtmlVideoAttributes = HtmlGlobalAttributes & {
  autoplay?: boolean;
  controls?: boolean;
  crossorigin?: "anonymous" | "use-credentials";
  height?: number;
  loop?: boolean;
  muted?: boolean;
  playsinline?: boolean;
  poster?: string;
  preload?: "auto" | "metadata" | "none";
  src?: string;
  width?: number;
};

export type MathmlGlobalAttributes = {
  class?: string;
  dir?: "ltr" | "rtl";
  displaystyle?: boolean;
  id?: string;
  nonce?: string;
  scriptlevel?: string;
  style?: string;
  tabindex?: number;
};

type Children = VNodeChild | VNodeChild[];

/** Returns HTML element `<a>` */
export function a(children?: Children): VNode;
export function a(
  attributes?: HtmlAnchorAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function a(
  arg1?: HtmlAnchorAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("a", arg1, arg2);
}

/** Returns HTML element `<abbr>` */
export function abbr(children?: Children): VNode;
export function abbr(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function abbr(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("abbr", arg1, arg2);
}

/** Returns HTML element `<address>` */
export function address(children?: Children): VNode;
export function address(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function address(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("address", arg1, arg2);
}

/** Returns HTML element `<area>` */
export function area(attributes?: HtmlAreaAttributes | VNodeAttributes): VNode {
  return h("area", attributes);
}

/** Returns HTML element `<article>` */
export function article(children?: Children): VNode;
export function article(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function article(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("article", arg1, arg2);
}

/** Returns HTML element `<aside>` */
export function aside(children?: Children): VNode;
export function aside(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function aside(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("aside", arg1, arg2);
}

/** Returns HTML element `<audio>` */
export function audio(children?: Children): VNode;
export function audio(
  attributes?: HtmlAudioAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function audio(
  arg1?: HtmlAudioAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("audio", arg1, arg2);
}

/** Returns HTML element `<b>` */
export function b(children?: Children): VNode;
export function b(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function b(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("b", arg1, arg2);
}

/** Returns HTML element `<base>` */
export function base(attributes?: HtmlBaseAttributes | VNodeAttributes): VNode {
  return h("base", attributes);
}

/** Returns HTML element `<bdi>` */
export function bdi(children?: Children): VNode;
export function bdi(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function bdi(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("bdi", arg1, arg2);
}

/** Returns HTML element `<bdo>` */
export function bdo(children?: Children): VNode;
export function bdo(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function bdo(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("bdo", arg1, arg2);
}

/** Returns HTML element `<blockquote>` */
export function blockquote(children?: Children): VNode;
export function blockquote(
  attributes?: HtmlBlockquoteAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function blockquote(
  arg1?: HtmlBlockquoteAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("blockquote", arg1, arg2);
}

/** Returns HTML element `<body>` */
export function body(children?: Children): VNode;
export function body(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function body(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("body", arg1, arg2);
}

/** Returns HTML element `<br>` */
export function br(attributes?: HtmlGlobalAttributes | VNodeAttributes): VNode {
  return h("br", attributes);
}

/** Returns HTML element `<button>` */
export function button(children?: Children): VNode;
export function button(
  attributes?: HtmlButtonAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function button(
  arg1?: HtmlButtonAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("button", arg1, arg2);
}

export type HtmlCanvasAttributes = HtmlGlobalAttributes & {
  height?: number;
  width?: number;
};

/** Returns HTML element `<canvas>` */
export function canvas(children?: Children): VNode;
export function canvas(
  attributes?: HtmlCanvasAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function canvas(
  arg1?: HtmlCanvasAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("canvas", arg1, arg2);
}

/** Returns HTML element `<caption>` */
export function caption(children?: Children): VNode;
export function caption(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function caption(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("caption", arg1, arg2);
}

/** Returns HTML element `<cite>` */
export function cite(children?: Children): VNode;
export function cite(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function cite(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("cite", arg1, arg2);
}

/** Returns HTML element `<code>` */
export function code(children?: Children): VNode;
export function code(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function code(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("code", arg1, arg2);
}

/** Returns HTML element `<col>` */
export function col(attributes?: HtmlColAttributes | VNodeAttributes): VNode {
  return h("col", attributes);
}

/** Returns HTML element `<colgroup>` */
export function colgroup(children?: Children): VNode;
export function colgroup(
  attributes?: HtmlColGroupAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function colgroup(
  arg1?: HtmlColGroupAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("colgroup", arg1, arg2);
}

/** Returns HTML element `<data>` */
export function data(children?: Children): VNode;
export function data(
  attributes?: HtmlDataAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function data(
  arg1?: HtmlDataAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("data", arg1, arg2);
}

/** Returns HTML element `<datalist>` */
export function datalist(children?: Children): VNode;
export function datalist(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function datalist(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("datalist", arg1, arg2);
}

/** Returns HTML element `<dd>` */
export function dd(children?: Children): VNode;
export function dd(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function dd(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("dd", arg1, arg2);
}

/** Returns HTML element `<del>` */
export function del(children?: Children): VNode;
export function del(
  attributes?: HtmlDelAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function del(
  arg1?: HtmlDelAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("del", arg1, arg2);
}

/** Returns HTML element `<details>` */
export function details(children?: Children): VNode;
export function details(
  attributes?: HtmlDetailsAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function details(
  arg1?: HtmlDetailsAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("details", arg1, arg2);
}

/** Returns HTML element `<dfn>` */
export function dfn(children?: Children): VNode;
export function dfn(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function dfn(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("dfn", arg1, arg2);
}

/** Returns HTML element `<dialog>` */
export function dialog(children?: Children): VNode;
export function dialog(
  attributes?: HtmlDialogAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function dialog(
  arg1?: HtmlDialogAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("dialog", arg1, arg2);
}

/** Returns HTML element `<div>` */
export function div(children?: Children): VNode;
export function div(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function div(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("div", arg1, arg2);
}

/** Returns HTML element `<dl>` */
export function dl(children?: Children): VNode;
export function dl(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function dl(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("dl", arg1, arg2);
}

/** Returns HTML element `<dt>` */
export function dt(children?: Children): VNode;
export function dt(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function dt(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("dt", arg1, arg2);
}

/** Returns HTML element `<em>` */
export function em(children?: Children): VNode;
export function em(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function em(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("em", arg1, arg2);
}

/** Returns HTML element `<embed>` */
export function embed(
  attributes?: HtmlEmbedAttributes | VNodeAttributes,
): VNode {
  return h("embed", attributes);
}

/** Returns HTML element `<fieldset>` */
export function fieldset(children?: Children): VNode;
export function fieldset(
  attributes?: HtmlFieldsetAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function fieldset(
  arg1?: HtmlFieldsetAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("fieldset", arg1, arg2);
}

/** Returns HTML element `<figcaption>` */
export function figcaption(children?: Children): VNode;
export function figcaption(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function figcaption(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("figcaption", arg1, arg2);
}

/** Returns HTML element `<figure>` */
export function figure(children?: Children): VNode;
export function figure(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function figure(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("figure", arg1, arg2);
}

/** Returns HTML element `<footer>` */
export function footer(children?: Children): VNode;
export function footer(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function footer(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("footer", arg1, arg2);
}

/** Returns HTML element `<form>` */
export function form(children?: Children): VNode;
export function form(
  attributes?: HtmlFormAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function form(
  arg1?: HtmlFormAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("form", arg1, arg2);
}

/** Returns HTML element `<h1>` */
export function h1(children?: Children): VNode;
export function h1(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h1(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h1", arg1, arg2);
}

/** Returns HTML element `<h2>` */
export function h2(children?: Children): VNode;
export function h2(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h2(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h2", arg1, arg2);
}

/** Returns HTML element `<h3>` */
export function h3(children?: Children): VNode;
export function h3(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h3(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h3", arg1, arg2);
}

/** Returns HTML element `<h4>` */
export function h4(children?: Children): VNode;
export function h4(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h4(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h4", arg1, arg2);
}

/** Returns HTML element `<h5>` */
export function h5(children?: Children): VNode;
export function h5(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h5(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h5", arg1, arg2);
}

/** Returns HTML element `<h6>` */
export function h6(children?: Children): VNode;
export function h6(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function h6(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("h6", arg1, arg2);
}

/** Returns HTML element `<head>` */
export function head(children?: Children): VNode;
export function head(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function head(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("head", arg1, arg2);
}

/** Returns HTML element `<header>` */
export function header(children?: Children): VNode;
export function header(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function header(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("header", arg1, arg2);
}

/** Returns HTML element `<hr>` */
export function hr(attributes?: HtmlGlobalAttributes | VNodeAttributes): VNode {
  return h("hr", attributes);
}

/** Returns HTML element `<html>` */
export function html(children?: Children): VNode;
export function html(
  attributes?: HtmlHtmlAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function html(
  arg1?: HtmlHtmlAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("html", arg1, arg2);
}

/** Returns HTML element `<i>` */
export function i(children?: Children): VNode;
export function i(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function i(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("i", arg1, arg2);
}

/** Returns HTML element `<iframe>` */
export function iframe(children?: Children): VNode;
export function iframe(
  attributes?: HtmlIframeAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function iframe(
  arg1?: HtmlIframeAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("iframe", arg1, arg2);
}

/** Returns HTML element `<img>` */
export function img(attributes?: HtmlImgAttributes | VNodeAttributes): VNode {
  return h("img", attributes);
}

/** Returns HTML element `<input>` */
export function input(
  attributes?: HtmlInputAttributes | VNodeAttributes,
): VNode {
  return h("input", attributes);
}

/** Returns HTML element `<ins>` */
export function ins(children?: Children): VNode;
export function ins(
  attributes?: HtmlInsAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function ins(
  arg1?: HtmlInsAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("ins", arg1, arg2);
}

/** Returns HTML element `<kbd>` */
export function kbd(children?: Children): VNode;
export function kbd(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function kbd(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("kbd", arg1, arg2);
}

/** Returns HTML element `<label>` */
export function label(children?: Children): VNode;
export function label(
  attributes?: HtmlLabelAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function label(
  arg1?: HtmlLabelAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("label", arg1, arg2);
}

/** Returns HTML element `<legend>` */
export function legend(children?: Children): VNode;
export function legend(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function legend(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("legend", arg1, arg2);
}

/** Returns HTML element `<li>` */
export function li(children?: Children): VNode;
export function li(
  attributes?: HtmlLiAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function li(
  arg1?: HtmlLiAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("li", arg1, arg2);
}

/** Returns HTML element `<link>` */
export function link(attributes?: HtmlLinkAttributes | VNodeAttributes): VNode {
  return h("link", attributes);
}

/** Returns HTML element `<main>` */
export function main(children?: Children): VNode;
export function main(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function main(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("main", arg1, arg2);
}

/** Returns HTML element `<map>` */
export function map(children?: Children): VNode;
export function map(
  attributes?: HtmlMapAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function map(
  arg1?: HtmlMapAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("map", arg1, arg2);
}

/** Returns HTML element `<mark>` */
export function mark(children?: Children): VNode;
export function mark(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function mark(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("mark", arg1, arg2);
}

/** Returns HTML element `<math>` */
export function math(children?: Children): VNode;
export function math(
  attributes?: HtmlMathAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function math(
  arg1?: HtmlMathAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("math", arg1, arg2);
}

/** Returns HTML element `<menu>` */
export function menu(children?: Children): VNode;
export function menu(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function menu(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("menu", arg1, arg2);
}

/** Returns HTML element `<meta>` */
export function meta(attributes?: HtmlMetaAttributes | VNodeAttributes): VNode {
  return h("meta", attributes);
}

/** Returns HTML element `<meter>` */
export function meter(children?: Children): VNode;
export function meter(
  attributes?: HtmlMeterAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function meter(
  arg1?: HtmlMeterAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("meter", arg1, arg2);
}

/** Returns HTML element `<nav>` */
export function nav(children?: Children): VNode;
export function nav(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function nav(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("nav", arg1, arg2);
}

/** Returns HTML element `<noscript>` */
export function noscript(children?: Children): VNode;
export function noscript(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function noscript(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("noscript", arg1, arg2);
}

/** Returns HTML element `<object>` */
export function object(children?: Children): VNode;
export function object(
  attributes?: HtmlObjectAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function object(
  arg1?: HtmlObjectAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("object", arg1, arg2);
}

/** Returns HTML element `<ol>` */
export function ol(children?: Children): VNode;
export function ol(
  attributes?: HtmlOlAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function ol(
  arg1?: HtmlOlAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("ol", arg1, arg2);
}

/** Returns HTML element `<optgroup>` */
export function optgroup(children?: Children): VNode;
export function optgroup(
  attributes?: HtmlOptGroupAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function optgroup(
  arg1?: HtmlOptGroupAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("optgroup", arg1, arg2);
}

/** Returns HTML element `<option>` */
export function option(children?: Children): VNode;
export function option(
  attributes?: HtmlOptionAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function option(
  arg1?: HtmlOptionAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("option", arg1, arg2);
}

/** Returns HTML element `<output>` */
export function output(children?: Children): VNode;
export function output(
  attributes?: HtmlOutputAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function output(
  arg1?: HtmlOutputAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("output", arg1, arg2);
}

/** Returns HTML element `<p>` */
export function p(children?: Children): VNode;
export function p(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function p(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("p", arg1, arg2);
}

/** Returns HTML element `<picture>` */
export function picture(children?: Children): VNode;
export function picture(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function picture(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("picture", arg1, arg2);
}

/** Returns HTML element `<portal>` */
export function portal(children?: Children): VNode;
export function portal(
  attributes?: HtmlPortalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function portal(
  arg1?: HtmlPortalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("portal", arg1, arg2);
}

/** Returns HTML element `<pre>` */
export function pre(children?: Children): VNode;
export function pre(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function pre(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("pre", arg1, arg2);
}

/** Returns HTML element `<progress>` */
export function progress(children?: Children): VNode;
export function progress(
  attributes?: HtmlProgressAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function progress(
  arg1?: HtmlProgressAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("progress", arg1, arg2);
}

/** Returns HTML element `<q>` */
export function q(children?: Children): VNode;
export function q(
  attributes?: HtmlQAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function q(
  arg1?: HtmlQAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("q", arg1, arg2);
}

/** Returns HTML element `<rp>` */
export function rp(children?: Children): VNode;
export function rp(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function rp(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("rp", arg1, arg2);
}

/** Returns HTML element `<rt>` */
export function rt(children?: Children): VNode;
export function rt(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function rt(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("rt", arg1, arg2);
}

/** Returns HTML element `<ruby>` */
export function ruby(children?: Children): VNode;
export function ruby(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function ruby(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("ruby", arg1, arg2);
}

/** Returns HTML element `<s>` */
export function s(children?: Children): VNode;
export function s(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function s(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("s", arg1, arg2);
}

/** Returns HTML element `<samp>` */
export function samp(children?: Children): VNode;
export function samp(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function samp(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("samp", arg1, arg2);
}

/** Returns HTML element `<script>` */
export function script(children?: Children): VNode;
export function script(
  attributes?: HtmlScriptAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function script(
  arg1?: HtmlScriptAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("script", arg1, arg2);
}

/** Returns HTML element `<section>` */
export function section(children?: Children): VNode;
export function section(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function section(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("section", arg1, arg2);
}

/** Returns HTML element `<select>` */
export function select(children?: Children): VNode;
export function select(
  attributes?: HtmlSelectAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function select(
  arg1?: HtmlSelectAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("select", arg1, arg2);
}

export type HtmlSlotAttributes = HtmlGlobalAttributes & {
  name?: string;
};

/** Returns HTML element `<slot>` */
export function slot(children?: Children): VNode;
export function slot(
  attributes?: HtmlSlotAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function slot(
  arg1?: HtmlSlotAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("slot", arg1, arg2);
}

/** Returns HTML element `<small>` */
export function small(children?: Children): VNode;
export function small(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function small(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("small", arg1, arg2);
}

/** Returns HTML element `<source>` */
export function source(
  attributes?: HtmlSourceAttributes | VNodeAttributes,
): VNode {
  return h("source", attributes);
}

/** Returns HTML element `<span>` */
export function span(children?: Children): VNode;
export function span(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function span(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("span", arg1, arg2);
}

/** Returns HTML element `<strong>` */
export function strong(children?: Children): VNode;
export function strong(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function strong(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("strong", arg1, arg2);
}

/** Returns HTML element `<style>` */
export function style(children?: Children): VNode;
export function style(
  attributes?: HtmlStyleAttributes,
  children?: Children,
): VNode;
export function style(
  arg1?: HtmlStyleAttributes | Children,
  arg2?: Children,
): VNode {
  return h("style", arg1, arg2);
}

/** Returns HTML element `<sub>` */
export function sub(children?: Children): VNode;
export function sub(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function sub(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("sub", arg1, arg2);
}

/** Returns HTML element `<summary>` */
export function summary(children?: Children): VNode;
export function summary(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function summary(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("summary", arg1, arg2);
}

/** Returns HTML element `<sup>` */
export function sup(children?: Children): VNode;
export function sup(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function sup(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("sup", arg1, arg2);
}

/** Returns HTML element `<svg>` */
export function svg(children?: Children): VNode;
export function svg(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function svg(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("svg", arg1, arg2);
}

/** Returns HTML element `<table>` */
export function table(children?: Children): VNode;
export function table(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function table(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("table", arg1, arg2);
}

/** Returns HTML element `<tbody>` */
export function tbody(children?: Children): VNode;
export function tbody(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function tbody(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("tbody", arg1, arg2);
}

/** Returns HTML element `<td>` */
export function td(children?: Children): VNode;
export function td(
  attributes?: HtmlTdAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function td(
  arg1?: HtmlTdAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("td", arg1, arg2);
}

/** Returns HTML element `<template>` */
export function template(children?: Children): VNode;
export function template(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function template(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("template", arg1, arg2);
}

/** Returns HTML element `<textarea>` */
export function textarea(children?: Children): VNode;
export function textarea(
  attributes?: HtmlTextareaAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function textarea(
  arg1?: HtmlTextareaAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("textarea", arg1, arg2);
}

/** Returns HTML element `<tfoot>` */
export function tfoot(children?: Children): VNode;
export function tfoot(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function tfoot(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("tfoot", arg1, arg2);
}

/** Returns HTML element `<th>` */
export function th(children?: Children): VNode;
export function th(
  attributes?: HtmlThAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function th(
  arg1?: HtmlThAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("th", arg1, arg2);
}

/** Returns HTML element `<thead>` */
export function thead(children?: Children): VNode;
export function thead(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function thead(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("thead", arg1, arg2);
}

/** Returns HTML element `<time>` */
export function time(children?: Children): VNode;
export function time(
  attributes?: HtmlTimeAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function time(
  arg1?: HtmlTimeAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("time", arg1, arg2);
}

/** Returns HTML element `<title>` */
export function title(children?: Children): VNode;
export function title(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function title(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("title", arg1, arg2);
}

/** Returns HTML element `<tr>` */
export function tr(children?: Children): VNode;
export function tr(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function tr(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("tr", arg1, arg2);
}

/** Returns HTML element `<track>` */
export function track(
  attributes?: HtmlTrackAttributes | VNodeAttributes,
): VNode {
  return h("track", attributes);
}

/** Returns HTML element `<u>` */
export function u(children?: Children): VNode;
export function u(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function u(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("u", arg1, arg2);
}

/** Returns HTML element `<ul>` */
export function ul(children?: Children): VNode;
export function ul(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function ul(
  arg1?: HtmlGlobalAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("ul", arg1, arg2);
}

/** Returns HTML element `<video>` */
export function video(children?: Children): VNode;
export function video(
  attributes?: HtmlVideoAttributes | VNodeAttributes,
  children?: Children,
): VNode;
export function video(
  arg1?: HtmlVideoAttributes | VNodeAttributes | Children,
  arg2?: Children,
): VNode {
  return h("video", arg1, arg2);
}

/** Returns HTML element `<wbr>` */
export function wbr(
  attributes?: HtmlGlobalAttributes | VNodeAttributes,
): VNode {
  return h("wbr", attributes);
}
