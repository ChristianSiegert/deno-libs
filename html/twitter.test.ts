import { assertEquals } from "../deps.ts";
import { Component } from "./component.ts";
import { createTwitterSummaryCard, createTwitterSummaryCardWithLargeImage } from "./twitter.ts";

Deno.test(createTwitterSummaryCard.name, () => {
  let component: Component = createTwitterSummaryCard({ title: "Lorem Ipsum" });
  assertEquals(
    component.toString(),
    '<meta content="summary" name="twitter:card">' +
      '<meta content="Lorem Ipsum" name="twitter:title">',
  );

  component = createTwitterSummaryCard({
    description: "Photos of all kinds of cute dogs",
    image: "http://example.com/dog.jpg",
    imageAlt: "A dog",
    site: "@username",
    title: "My Favorite Dogs",
  });
  assertEquals(
    component.toString(),
    '<meta content="summary" name="twitter:card">' +
      '<meta content="Photos of all kinds of cute dogs" name="twitter:description">' +
      '<meta content="http://example.com/dog.jpg" name="twitter:image">' +
      '<meta content="A dog" name="twitter:image:alt">' +
      '<meta content="@username" name="twitter:site">' +
      '<meta content="My Favorite Dogs" name="twitter:title">',
  );
});

Deno.test(createTwitterSummaryCardWithLargeImage.name, () => {
  let component: Component = createTwitterSummaryCardWithLargeImage({
    title: "Lorem Ipsum",
  });
  assertEquals(
    component.toString(),
    '<meta content="summary_large_image" name="twitter:card">' +
      '<meta content="Lorem Ipsum" name="twitter:title">',
  );

  component = createTwitterSummaryCardWithLargeImage({
    description: "Photos of all kinds of cute dogs",
    image: "http://example.com/dog.jpg",
    imageAlt: "A dog",
    site: "@username",
    title: "My Favorite Dogs",
  });
  assertEquals(
    component.toString(),
    '<meta content="summary_large_image" name="twitter:card">' +
      '<meta content="Photos of all kinds of cute dogs" name="twitter:description">' +
      '<meta content="http://example.com/dog.jpg" name="twitter:image">' +
      '<meta content="A dog" name="twitter:image:alt">' +
      '<meta content="@username" name="twitter:site">' +
      '<meta content="My Favorite Dogs" name="twitter:title">',
  );
});
