import { Component } from "./component.ts";
import { h, VNode } from "./VNode.ts";
import { assertEquals, assertThrows } from "../deps.ts";

const voidElements: string[] = [
  "area",
  "base",
  "br",
  "col",
  "embed",
  "hr",
  "img",
  "input",
  "link",
  "meta",
  "param",
  "source",
  "track",
  "wbr",
];

Deno.test(`${VNode.name}: toString()`, () => {
  // Renders tags
  const vnodes: VNode[] = [
    new VNode("div"),
    new VNode("div", undefined),
    new VNode("div", undefined, undefined),
    new VNode("div", undefined, []),
    new VNode("div", {}),
    new VNode("div", {}, undefined),
    new VNode("div", {}, []),
  ];
  vnodes.forEach((vnode) => assertEquals(vnode.toString(), "<div></div>"));

  // Renders attributes
  assertEquals(
    new VNode("div", {
      attr1: "text",
      attr2: "",
      attr3: 1,
      attr4: 0,
      attr5: NaN,
      attr6: Infinity,
      attr7: true,
      attr8: false,
      attr9: null,
      attr10: undefined,
    }).toString(),
    '<div attr1="text" attr2="" attr3="1" attr4="0" attr5="NaN" attr6="Infinity" attr7></div>',
  );

  // Renders child elements
  assertEquals(
    new VNode("a", {}, [
      "",
      null,
      undefined,
      new Component(),
      new VNode("b"),
    ]).toString(),
    "<a><b></b></a>",
  );

  assertEquals(
    new VNode("div", {}, [
      "Child 1",
      new VNode("b", {}, [
        "Child 2",
      ]),
      new VNode("ol", {}, [
        new VNode("li", {}, [
          new VNode("u", {}, [
            "Child 3.1.1",
          ]),
        ]),
      ]),
    ]).toString(),
    "<div>Child 1<b>Child 2</b><ol><li><u>Child 3.1.1</u></li></ol></div>",
  );

  // Does not render element when attribute "if" exists and is falsy
  [
    new VNode("a", {}, ["Link"]),
    new VNode("a", { if: "text" }, ["Link"]),
    new VNode("a", { if: 1 }, ["Link"]),
    new VNode("a", { if: Infinity }, ["Link"]),
    new VNode("a", { if: true }, ["Link"]),
  ].forEach((vnode: VNode) => assertEquals(vnode.toString(), "<a>Link</a>"));

  [
    new VNode("a", { if: "" }, ["Link"]),
    new VNode("a", { if: 0 }, ["Link"]),
    new VNode("a", { if: NaN }, ["Link"]),
    new VNode("a", { if: false }, ["Link"]),
    new VNode("a", { if: null }, ["Link"]),
    new VNode("a", { if: undefined }, ["Link"]),
  ].forEach((vnode: VNode) => assertEquals(vnode.toString(), ""));

  assertEquals(
    new VNode("header", {}, [
      new VNode("a", {}, ["Link 1"]),
      new VNode("a", { if: "text" }, ["Link 2"]),
      new VNode("a", { if: "" }, ["Link 3"]),
      new VNode("a", { if: 1 }, ["Link 4"]),
      new VNode("a", { if: 0 }, ["Link 5"]),
      new VNode("a", { if: NaN }, ["Link 6"]),
      new VNode("a", { if: Infinity }, ["Link 7"]),
      new VNode("a", { if: true }, ["Link 8"]),
      new VNode("a", { if: false }, ["Link 9"]),
      new VNode("a", { if: null }, ["Link 10"]),
      new VNode("a", { if: undefined }, ["Link 11"]),
    ]).toString(),
    "<header><a>Link 1</a><a>Link 2</a><a>Link 4</a><a>Link 7</a><a>Link 8</a></header>",
  );

  // Escapes attribute values
  assertEquals(
    new VNode("input", {
      attr: '&reg;"attr 1"&reg;',
      class: { '&reg;"class1"&reg;': true, class2: true, class3: false },
    }).toString(),
    '<input attr="&amp;reg;&quot;attr 1&quot;&amp;reg;" class="&amp;reg;&quot;class1&quot;&amp;reg; class2">',
  );

  // Escapes text content
  assertEquals(
    new VNode("div", {}, ["<b>&lt;i&gt;Hello&lt;/i&gt;</b>"]).toString(),
    "<div>&lt;b&gt;&amp;lt;i&amp;gt;Hello&amp;lt;/i&amp;gt;&lt;/b&gt;</div>",
  );

  // Does not escape script element’s text content
  assertEquals(
    new VNode("script", {}, ["&<>"]).toString(),
    "<script>&<></script>",
  );

  // Does not escape style element’s text content
  assertEquals(
    new VNode("style", {}, ["ul > li { color: black };", "&<>"]).toString(),
    "<style>ul > li { color: black };&<></style>",
  );

  // Escapes text content in children
  assertEquals(
    new VNode("div", {}, [
      new VNode("b", {}, [
        new VNode("i", {}, ["Hey"]),
      ]),
      new VNode("b", {}, ["<i>Hi</i>"]),
      "<b><i>Hello</i></b>",
      "<b>&lt;i&gt;Goodbye&lt;/i&gt;</b>",
    ]).toString(),
    "<div>" +
      "<b><i>Hey</i></b>" +
      "<b>&lt;i&gt;Hi&lt;/i&gt;</b>" +
      "&lt;b&gt;&lt;i&gt;Hello&lt;/i&gt;&lt;/b&gt;" +
      "&lt;b&gt;&amp;lt;i&amp;gt;Goodbye&amp;lt;/i&amp;gt;&lt;/b&gt;" +
      "</div>",
  );

  // Sorts attributes
  assertEquals(
    new VNode("div", { c: "c1", d: "d1", b: "b1", a: "a1" }).toString(),
    '<div a="a1" b="b1" c="c1" d="d1"></div>',
  );

  // Sorts classes
  assertEquals(
    new VNode("div", { class: { b: true, c: false, d: true, a: true } })
      .toString(),
    '<div class="a b d"></div>',
  );

  // Applies all requirements combined
  assertEquals(
    new VNode("ol", {
      style: 'font-family: "Helvetica Neue"',
      class: {
        class2: true,
        class1: true,
        class3: false,
      },
    }, [
      new VNode("li", {}, ["<b>&lt;i&gt;Point 1&lt;/i&gt;</b>"]),
      new VNode("li", { if: true }, ["<b>Point 2</b>"]),
      new VNode("li", { if: false }, ["<b>Point 3</b>"]),
      "<li>&lt;b&gt;Point 4&lt;/b&gt;</li>",
    ]).toString(),
    '<ol class="class1 class2" style="font-family: &quot;Helvetica Neue&quot;">' +
      "<li>&lt;b&gt;&amp;lt;i&amp;gt;Point 1&amp;lt;/i&amp;gt;&lt;/b&gt;</li>" +
      "<li>&lt;b&gt;Point 2&lt;/b&gt;</li>" +
      "&lt;li&gt;&amp;lt;b&amp;gt;Point 4&amp;lt;/b&amp;gt;&lt;/li&gt;" +
      "</ol>",
  );

  // Does not escape content of attribute "unsafeHtml"
  assertEquals(
    new VNode("a", { unsafeHtml: "<b>unsafe</b>" }, ["ignored content"])
      .toString(),
    "<a><b>unsafe</b></a>",
  );

  // Does not render closing tag for void element
  voidElements.forEach((elementName) => {
    assertEquals(new VNode(elementName).toString(), `<${elementName}>`);
  });

  // Throws when content is provided for void element
  voidElements.forEach((elementName) => {
    assertThrows(() => new VNode(elementName, {}, [new VNode("b")]));
    assertThrows(() => new VNode(elementName, {}, ["text"]));
    assertThrows(() => new VNode(elementName, {}, [""]));
    assertThrows(() => new VNode(elementName, {}, [null]));
    assertThrows(() => new VNode(elementName, {}, [undefined]));
  });

  // Accepts components as children
  assertEquals(
    new VNode("a", {}, [
      new Component([new VNode("b", {}, ["text 1"])]),
      new Component([new VNode("b", {}, ["text 2"])]),
      h("b", "text 3"),
      "text 4",
    ]).toString(),
    "<a><b>text 1</b><b>text 2</b><b>text 3</b>text 4</a>",
  );
});

Deno.test(h.name, () => {
  const tests: [vnode: VNode, expected: string][] = [
    [h("a"), "<a></a>"],

    // Second argument: children
    [h("a", null), "<a></a>"],
    [h("a", undefined), "<a></a>"],
    [h("a", "Text 1"), "<a>Text 1</a>"],
    [h("a", h("b")), "<a><b></b></a>"],
    [h("a", new Component([h("b")])), "<a><b></b></a>"],
    [
      h("a", [
        null,
        undefined,
        "Text 1",
        h("b", "Text 2"),
        new Component([null, undefined, "Text 3", h("b", "Text 4")]),
      ]),
      "<a>Text 1<b>Text 2</b>Text 3<b>Text 4</b></a>",
    ],

    // Second argument: attributes
    [h("a", { attr: "value" }), '<a attr="value"></a>'],
    [h("a", { attr: "value" }, null), '<a attr="value"></a>'],
    [h("a", { attr: "value" }, undefined), '<a attr="value"></a>'],
    [h("a", { attr: "value" }, "Text 1"), '<a attr="value">Text 1</a>'],
    [h("a", { attr: "value" }, h("b")), '<a attr="value"><b></b></a>'],
    [
      h("a", { attr: "value" }, new Component([h("b")])),
      '<a attr="value"><b></b></a>',
    ],
    [
      h("a", { attr: "value" }, [
        null,
        undefined,
        "Text 1",
        h("b", { attr: "value" }, "Text 2"),
        new Component([null, undefined, "Text 3", h("b", { a: 2 }, "Text 4")]),
      ]),
      '<a attr="value">Text 1<b attr="value">Text 2</b>Text 3<b a="2">Text 4</b></a>',
    ],
  ];
  tests.forEach(([vnode, expected]) => assertEquals(vnode.toString(), expected));
});
