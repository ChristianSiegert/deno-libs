/// <reference lib="dom" />

addEventListener("DOMContentLoaded", () => {
  "use strict";
  const popupSelector = "details.cs-popup[open]";
  let touchstart = 0;

  addEventListener("click", ({ target }: MouseEvent) => {
    closeIfNotTarget(target);
  });

  addEventListener("keyup", (event: KeyboardEvent) => {
    if (event.key !== "Escape") return;
    closeIfNotTarget(null);
  });

  addEventListener("touchstart", () => {
    touchstart = Date.now();
  });

  addEventListener("touchend", ({ target }: TouchEvent) => {
    if (Date.now() - touchstart > 500) return;
    closeIfNotTarget(target);
  });

  const options: AddEventListenerOptions = { once: true, passive: true };
  addEventListener("scroll", closeOnScroll, options);

  function closeOnScroll() {
    closeIfNotTarget(null);
    setTimeout(() => addEventListener("scroll", closeOnScroll, options), 500);
  }

  function closeIfNotTarget(target: EventTarget | null) {
    document.querySelectorAll<HTMLDetailsElement>(popupSelector).forEach((popup) => {
      if (target instanceof Element && target.closest(popupSelector) === popup) return;
      popup.open = false;
    });
  }
});
