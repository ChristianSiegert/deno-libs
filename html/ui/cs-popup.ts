import { lazyCache } from "../../caching/cache.ts";
import { css } from "../../css/css.ts";
import { Component } from "../../html/component.ts";
import { details, div } from "../../html/elements.ts";

/**
 * csPopup returns a component that displays a button. When clicking
 * on the button, a popup opens below the button. The popup works also in
 * browsers where JavaScript is turned off. If JavaScript is turned on, the
 * popup can be additionally closed by pressing Esc or clicking outside of the
 * popup. The button must be a `<summary>` element.
 * @param options.button Must be `<summary>` element
 * @param options.id Deprecated. ID to give `<details>` element.
 * @param options.if Wether to show button. If true or undefined, button is shown.
 * @param options.popup Content of popup
 * @param options.popupAnchor Align popup left or right below button
 * @param options.popupIsOpen Wether to show popup immediately without user interaction.
 * @unstable
 */
export function csPopup(options: {
  button: Component;
  if?: boolean;
  popup: Component;
  popupAnchor?: "left" | "right";
  popupIsOpen?: boolean;
}): Component {
  if (options.if === false) return new Component([]);
  return new Component([
    details({
      class: {
        "cs-popup": true,
        "cs-popup--anchor-right": options.popupAnchor === "right",
      },
      open: options.popupIsOpen,
    }, [
      options.button,
      div({ class: "cs-popup__body" }, [options.popup]),
    ]),
  ], { script, style: style() });
}

// deno-fmt-ignore
const style = lazyCache(() => css`
.cs-popup--anchor-right {
  position: relative;
}
.cs-popup__body {
  margin-top: 0.25rem;
  max-width: calc(100vw - 5rem);
  position: absolute;
  width: max-content;
  z-index: 99;
}
.cs-popup--anchor-right > .cs-popup__body {
  right: 0;
}
.cs-popup > summary { list-style-type: none }
.cs-popup > summary::-webkit-details-marker { display: none }
`);

// Minified content of ./cs-popup.client.ts
const script =
  'addEventListener("DOMContentLoaded",()=>{"use strict";const n="details.cs-popup[open]";let o=0;addEventListener("click",({target:e})=>{t(e)}),addEventListener("keyup",e=>{e.key==="Escape"&&t(null)}),addEventListener("touchstart",()=>{o=Date.now()}),addEventListener("touchend",({target:e})=>{Date.now()-o>500||t(e)});const s={once:!0,passive:!0};addEventListener("scroll",r,s);function r(){t(null),setTimeout(()=>addEventListener("scroll",r,s),500)}function t(e){document.querySelectorAll(n).forEach(l=>{e instanceof Element&&e.closest(n)===l||(l.open=!1)})}});';

// Uncomment below while developing
// const script = await (
//   await fetch(import.meta.resolve("./popup.client.min.js"))
// ).text();
