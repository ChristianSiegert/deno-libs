import { lazyCache } from "../../caching/cache.ts";
import { css } from "../../css/css.ts";
import { Component } from "../../html/component.ts";
import { div, li, ol } from "../../html/elements.ts";
import { csIonIconV7 } from "../../html/ui/cs-ion-icon-v7.ts";

export type CsNotification = {
  text?: string;
  title?: string;
  type?: "info" | "warning";
};

/**
 * Returns a component that displays notifications to the user. The close icon
 * must be located in a readable directory named `public/ionicons-v7`.
 * [Download icons](https://ionic.io/ionicons/ionicons.designerpack.zip).
 */
export async function csNotifications(options: {
  /** Text to display when hovering over close button. */
  closeLabel?: string;
  /** Notifications to display. */
  notifications: CsNotification[];
}): Promise<Component | undefined> {
  if (!options.notifications.length) return;
  return new Component([
    ol(
      { class: "cs-notifications" },
      await Promise.all(options.notifications.map(async (notification) =>
        li({
          class: {
            "cs-notification": true,
            "cs-notification--warning": notification.type === "warning",
          },
        }, [
          div({ class: "cs-notification__content" }, [
            div({
              if: notification.title,
              class: "cs-notification__title",
            }, notification.title),
            div({
              if: notification.text,
              class: "cs-notification__text",
            }, notification.text),
          ]),
          div({
            class: "cs-notification__close",
            onclick: 'event.target.closest(".cs-notification").remove()',
            title: options.closeLabel,
          }, await csIonIconV7({ name: "close", size: 14 })),
        ])
      )),
    ),
  ], { style: style() });
}

// deno-fmt-ignore
const style = lazyCache(() => css`
.cs-notifications {
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
  left: 0.5rem;
  margin: 0 auto;
  padding-left: 0;
  pointer-events: none;
  position: fixed;
  right: 0.5rem;
  top: 0.5rem;
  width: fit-content;
  z-index: 100;
}
.cs-notification {
  align-items: center;
  background-color: hsl(210 100% 55%);
  border-radius: 2px;
  box-shadow: 0px 3px 3px -2px hsl(0 0% 35% / 0.5);
  color: #fff;
  display: flex;
  font-size: 14px;
  gap: 0.5rem;
  padding: 0.375rem 0.75rem;
  pointer-events: auto;
}
.cs-notification--warning {
  background-color: hsl(0 30% 50%);
}
.cs-notification__content {
  flex-grow: 1;
}
.cs-notification__title {
  font-weight: 600;
}
.cs-notification__close {
  border-radius: 4px;
  cursor: pointer;
  line-height: 0;
  padding: 3px;
}
.cs-notification__close:hover {
  background: hsl(0 0% 100% / 0.15);
}
`);
