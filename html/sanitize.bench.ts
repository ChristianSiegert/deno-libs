const input = '<<hello>>""You"" & ""Me""<</hello>>'.repeat(100);

const replacementPattern = /[&<>]/g;

const replacements: Record<string, string> = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
};

function sanitizeTextContentWithRegEx(value: string) {
  return value.replaceAll(replacementPattern, (match) => replacements[match]);
}

function sanitizeTextContentWithReplaceAll(value: string): string {
  return value
    .replaceAll("&", "&amp;")
    .replaceAll("<", "&lt;")
    .replaceAll(">", "&gt;");
}

Deno.bench(sanitizeTextContentWithRegEx.name, () => {
  sanitizeTextContentWithRegEx(input);
});

Deno.bench(sanitizeTextContentWithReplaceAll.name, { baseline: true }, () => {
  sanitizeTextContentWithReplaceAll(input);
});
