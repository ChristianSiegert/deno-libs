import { Component } from "./component.ts";
import { VNode } from "./VNode.ts";

class BenchComponent extends Component {
  // Same as getScripts but returns Set<string> instead of string[]
  getScriptsSet(): Set<string> {
    const scripts = new Set<string>(this.scripts);

    this.content.forEach((item) => {
      if (item instanceof Component) {
        item.getScripts().forEach((script) => scripts.add(script));
      } else if (item instanceof VNode) {
        item.children.forEach((child) => {
          if (child instanceof Component) {
            child.getScripts().forEach((script) => scripts.add(script));
          }
        });
      }
    });

    return scripts;
  }

  // Same as getStyles but returns Set<string> instead of string[]
  getStylesSet(): Set<string> {
    const styles = new Set<string>(this.styles);

    this.content.forEach((item) => {
      if (item instanceof Component) {
        item.getStyles().forEach((style) => styles.add(style));
      } else if (item instanceof VNode) {
        item.children.forEach((child) => {
          if (child instanceof Component) {
            child.getStyles().forEach((style) => styles.add(style));
          }
        });
      }
    });

    return styles;
  }
}

const style = ".a { color: #aaa };".repeat(50);
const script = "alert('hi');".repeat(50);

const components = new BenchComponent([
  new BenchComponent([
    new BenchComponent([
      new BenchComponent([
        new BenchComponent([], { style, script }),
      ], { style, script }),
    ], { style, script }),
  ], { style, script }),
], { style, script });

Deno.bench(Component.name, () => {
  new Component([]);
});

Deno.bench(`${Component.name} getScripts()`, { group: "getScripts", baseline: true }, () => {
  components.getScripts();
});

Deno.bench(`${Component.name} getScriptsSet()`, { group: "getScripts" }, () => {
  components.getScriptsSet();
});

Deno.bench(`${Component.name} getStyles()`, { group: "getStyles", baseline: true }, () => {
  components.getStyles();
});

Deno.bench(`${Component.name} getStylesSet()`, { group: "getStyles" }, () => {
  components.getStylesSet();
});

const component = new Component(
  Array.from({ length: 10 }).map(() =>
    new Component(
      Array.from({ length: 10 }).map(() =>
        new Component(
          Array.from({ length: 10 }).map(() =>
            new Component(
              Array.from({ length: 10 }).map(() => new Component()),
            )
          ),
        )
      ),
    )
  ),
);
Deno.bench(
  "Component.toString() with 11110 sub components",
  { group: "toString", baseline: true },
  () => {
    component.toString();
  },
);
