import { css } from "./css.ts";

Deno.bench(`${css.name} (50k lines)`, () => {
  css`
    ${"  .selector { key: value; }\n".repeat(50_000)}
  `;
});
