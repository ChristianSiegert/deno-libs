/**
 * Tag function that enables CSS syntax highlighting in the string literal if a
 * supporting editor extension is installed. Additionally, removes leading
 * whitespace from lines.
 */
export function css(
  strings: TemplateStringsArray,
  ...values: unknown[]
): string {
  let result = "";
  strings.forEach((string, index) =>
    result += index < values.length ? string + values[index] : string
  );
  return result.replaceAll(/\n\s*/g, "");
}
