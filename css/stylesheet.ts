export type Style = { [propertyName: string]: number | string | string[] };
export type Styles = { [selector: string]: Style };

/**
 * Class Stylesheet takes an object of CSS selector-rule pairs and when
 * stringified returns compact CSS without the need for additional tooling or a
 * build step.
 * @example
 * ```ts
 * const stylesheet = new Stylesheet({
 *   "h1": {
 *     "cursor": "default",
 *     "width": ["-moz-fit-content", "fit-content"],
 *   },
 *   "section > h1": {
 *     "font-size": "20px"
 *   },
 * });
 *
 * stylesheet.toString()
 * // h1{cursor:default;width:-moz-fit-content;width:fit-content}
 * // section > h1{font-size:20px}
 * ```
 * @unstable
 */
export class Stylesheet {
  styles: Styles;

  constructor(styles: Styles = {}) {
    this.styles = styles;
  }

  toString(): string {
    let string = "";
    Object.entries(this.styles).forEach(([selector, style]) => {
      string += `${selector}{${this.#styleToString(style)}}`;
    });
    return string;
  }

  #styleToString(style: Style): string {
    let string = "";
    Object.entries(style).forEach(([name, value], index) => {
      if (index > 0) string += ";";
      if (Array.isArray(value)) {
        value.forEach((value_, index) => {
          if (index > 0) string += ";";
          string += `${name}:${value_}`;
        });
      } else {
        string += `${name}:${value}`;
      }
    });
    return string;
  }
}
