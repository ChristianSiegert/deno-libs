import { type Style, type Styles, Stylesheet } from "./stylesheet.ts";

/**
 * [JavaScript template literal tag](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#tagged_templates)
 * @unstable
 */
export type TemplateLiteralTag = (
  strings: TemplateStringsArray,
  ...values: unknown[]
) => string;

/**
 * createU returns a JavaScript template literal tag that is bound to the
 * provided UtilityStylesheet. Using this tag allows to create utility css
 * classes without having to use the longer `stylesheet.ucss()` and often
 * results in fewer lines.
 * @example
 * ```ts
 * const stylesheet = new UtilityStylesheet();
 * const u = createU(stylesheet);
 * u`display:flex gap:[5px 10px] m-4 width:75%`;
 * stylesheet.toString();
 * ```
 * @unstable
 */
export function createU(stylesheet: UtilityStylesheet): TemplateLiteralTag {
  return function (
    strings: TemplateStringsArray,
    ...values: unknown[]
  ): string {
    const properties = strings.map((s, index) => index < values.length ? s + values[index] : s)
      .join("");
    return stylesheet.ucss(properties);
  };
}

/**
 * UtilityStylesheet creates CSS utility classes from a list of CSS properties.
 * See method `ucss()`
 * @example
 * ```ts
 * const stylesheet = new UtilityStylesheet()
 * stylesheet.ucss("display:flex gap:[5px 10px] m-4 width:75%")
 * stylesheet.toString()
 * // .display\:flex{display:flex}
 * // .gap\:5px-10px{gap:5px 10px}
 * // .m-4{margin:1rem}
 * // .width\:75\%{width:75%}
 * ```
 * @unstable
 */
export class UtilityStylesheet extends Stylesheet {
  colorSchemeStyles: { [key: string]: Styles } = {
    "": {},
    "dark": {},
    "light": {},
  };

  static #propertyAliases: { [key: string]: string[] } = {
    "m": ["margin"],
    "mb": ["margin-bottom"],
    "ml": ["margin-left"],
    "mr": ["margin-right"],
    "mt": ["margin-top"],
    "mx": ["margin-left", "margin-right"],
    "my": ["margin-bottom", "margin-top"],
    "p": ["padding"],
    "pb": ["padding-bottom"],
    "pl": ["padding-left"],
    "pr": ["padding-right"],
    "pt": ["padding-top"],
    "px": ["padding-left", "padding-right"],
    "py": ["padding-bottom", "padding-top"],
  };

  static #allowedStatePrefixes = new Set([
    "active",
    "focus",
    "focus-visible",
    "focus-within",
    "hover",
    "target",
    "visited",
  ]);

  static #allowedColorSchemePrefixes = new Set(["dark", "light"]);

  static #selectorEscapeChars = /([^a-zA-Z0-9_-])/g;

  static #sizePattern = /^([a-z-]+)\-([0-9]+(?:\.[0-9]+)?)(!)?$/;

  static #vendorPrefixes: Map<string, {
    namePrefixes?: string[];
    valuePrefixes?: Map<string, string[]>;
  }> = new Map([
    ["backdrop-filter", {
      namePrefixes: ["-webkit-"],
    }],
    ["position", {
      valuePrefixes: new Map([
        ["sticky", ["-webkit-"]],
      ]),
    }],
    ["user-select", {
      namePrefixes: ["-webkit-"],
    }],
    ["width", {
      valuePrefixes: new Map([
        ["fit-content", ["-moz-"]],
        ["max-content", ["-moz-"]],
        ["min-content", ["-moz-"]],
      ]),
    }],
  ]);

  constructor(styles: Styles = {}) {
    super(styles);
  }

  override toString(): string {
    return Object.entries(this.colorSchemeStyles)
      .filter(([_colorScheme, styles]) => Object.keys(styles).length)
      .flatMap(([colorScheme, styles]) => [
        colorScheme ? `@media(prefers-color-scheme:${colorScheme}){` : "",
        ...Object.entries(styles).sort().map(([selector, style]) =>
          `${selector}${this.#styleToString(style)}`
        ),
        colorScheme ? "}" : "",
      ])
      .join("");
  }

  /**
   * ucss takes a space-separated list of CSS properties, creates utility
   * classes from it, adds the classes to the stylesheet and then returns the
   * class names. If a property has multiple values, they must be grouped in
   * brackets. CSS properties margin and padding have the following aliases:
   * `m mb ml mr mt mx my p pb pl pr pt px py`.
   * @example
   * ```ts
   * stylesheet.ucss("display:flex gap:[5px 10px] m-4 width:75%")
   * ```
   * @param properties Space-separated list of CSS properties
   * @returns Space-separated list of class names for use in HTML `class` attribute
   */
  ucss(properties: string): string {
    const classNames: string[] = [];
    for (const property of this.#splitPropertyList(properties)) {
      const statePrefixes: string[] = [];
      let colorSchemePrefix = "";
      let name: string | undefined;
      let size: number | undefined;
      let important: boolean | undefined;
      let value: string | undefined;

      const parts = property.split(":").map((part) => part.trim())
        .filter((part) => part);
      if (!parts.length) continue;

      let prefixes: string[];
      const lastPart = parts[parts.length - 1];
      const sizeMatch = UtilityStylesheet.#sizePattern.exec(lastPart);

      if (sizeMatch) {
        name = sizeMatch[1];
        size = Number.parseFloat(sizeMatch[2]);
        important = !!sizeMatch[3];
        value = (size * 0.25) + "rem";
        if (important) value += " !important";
        prefixes = parts.slice(0, -1);
      } else {
        if (parts.length < 2) continue;
        name = parts[parts.length - 2];
        value = parts[parts.length - 1];
        prefixes = parts.slice(0, -2);
      }

      for (const prefix of prefixes) {
        if (UtilityStylesheet.#allowedStatePrefixes.has(prefix)) {
          statePrefixes.push(prefix);
        } else if (UtilityStylesheet.#allowedColorSchemePrefixes.has(prefix)) {
          colorSchemePrefix = prefix;
        }
      }

      const selector = "." + [
        ...(colorSchemePrefix ? [colorSchemePrefix] : []),
        ...statePrefixes,
        size === undefined
          ? `${name}\\:${
            value
              .replaceAll(" ", "-")
              .replaceAll(UtilityStylesheet.#selectorEscapeChars, "\\$1")
          }`
          : `${name}-${size}${important ? "!" : ""}`
            .replaceAll(UtilityStylesheet.#selectorEscapeChars, "\\$1"),
      ].join("\\:") +
        statePrefixes.map((state) => ":" + state).join("");

      this.colorSchemeStyles[colorSchemePrefix][selector] = {};
      const vendorEntry = UtilityStylesheet.#vendorPrefixes.get(name);
      const namePrefixes = vendorEntry?.namePrefixes ?? [];
      const valuePrefixes = vendorEntry?.valuePrefixes?.get(value) ?? [];

      for (const namePrefix of [...namePrefixes, ""]) {
        const propertyNames = UtilityStylesheet.#propertyAliases[name] ??
          [namePrefix + name];
        for (const propertyName of propertyNames) {
          this.colorSchemeStyles[colorSchemePrefix][selector][propertyName] = [
            ...valuePrefixes.map((prefix) => prefix + value),
            value,
          ];
        }
      }

      classNames.push([
        ...(colorSchemePrefix ? [colorSchemePrefix] : []),
        ...statePrefixes,
        size === undefined
          ? `${name}:${value.replaceAll(" ", "-")}`
          : `${name}-${size}${important ? "!" : ""}`,
      ].join(":"));
    }
    return classNames.sort().join(" ");
  }

  #splitPropertyList(list: string): string[] {
    const properties: string[] = [];
    let property = "";
    let valueGroupDepth = 0;
    for (let i = 0, { length } = list; i < length; i++) {
      const char = list[i];
      if (char === "(") {
        valueGroupDepth += 1;
        property += char;
      } else if (char === ")") {
        valueGroupDepth -= 1;
        property += char;
      } else if (char === "[") {
        valueGroupDepth += 1;
      } else if (char === "]") {
        valueGroupDepth -= 1;
      } else if (char === " " && !valueGroupDepth) {
        if (property) properties.push(property);
        property = "";
      } else {
        property += char;
      }
    }
    if (property) properties.push(property);
    return properties;
  }

  #styleToString(style: Style): string {
    return "{" +
      Object.entries(style).flatMap(([name, value]) =>
        Array.isArray(value) ? value.map((value) => `${name}:${value}`) : `${name}:${value}`
      ).join(";") +
      "}";
  }
}
