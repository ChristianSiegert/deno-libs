import { assertEquals } from "../deps.ts";
import { Stylesheet } from "./stylesheet.ts";

Deno.test(Stylesheet.name, async (t) => {
  await t.step("toString()", () => {
    assertEquals(
      new Stylesheet({
        ".class1": {
          cursor: "default",
          width: ["-moz-fit-content", "fit-content"],
        },
        ".class2": {
          cursor: "default",
          width: ["-moz-fit-content", "fit-content"],
          "z-index": 2,
        },
      }).toString(),
      ".class1{cursor:default;width:-moz-fit-content;width:fit-content}" +
        ".class2{cursor:default;width:-moz-fit-content;width:fit-content;z-index:2}",
    );
  });
});
