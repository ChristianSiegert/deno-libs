import { assertEquals } from "../deps.ts";
import { UtilityStylesheet } from "./utility-stylesheet.ts";

Deno.test(UtilityStylesheet.name, async (t) => {
  await t.step("ucss()", () => {
    // Adds utility classes to stylesheet, returns utility class names
    let stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss("display:flex gap:10px"),
      "display:flex gap:10px",
    );
    assertEquals(
      stylesheet.toString(),
      ".display\\:flex{display:flex}.gap\\:10px{gap:10px}",
    );

    // Property value with spaces can be grouped with square brackets []
    stylesheet = new UtilityStylesheet();
    assertEquals(stylesheet.ucss("gap:[10px 20px]"), "gap:10px-20px");
    assertEquals(stylesheet.toString(), ".gap\\:10px-20px{gap:10px 20px}");

    // Property value with spaces can be grouped with round brackets ()
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss("color:rgb(0 0 0) width:calc(100vw - 2rem)"),
      "color:rgb(0-0-0) width:calc(100vw---2rem)",
    );
    assertEquals(
      stylesheet.toString(),
      ".color\\:rgb\\(0-0-0\\){color:rgb(0 0 0)}.width\\:calc\\(100vw---2rem\\){width:calc(100vw - 2rem)}",
    );

    // Property value groups can be nested
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss("filter:[drop-shadow(16px 16px 20px red) invert(75%)]"),
      "filter:drop-shadow(16px-16px-20px-red)-invert(75%)",
    );
    assertEquals(
      stylesheet.toString(),
      ".filter\\:drop-shadow\\(16px-16px-20px-red\\)-invert\\(75\\%\\){filter:drop-shadow(16px 16px 20px red) invert(75%)}",
    );

    // Escape special characters in selector
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss(
        "aspect-ratio:16/9 color:[#fff !important] filter:[blur(10%) sepia(30%)] grid-column:1/-1 width:33.3%",
      ),
      "aspect-ratio:16/9 color:#fff-!important filter:blur(10%)-sepia(30%) grid-column:1/-1 width:33.3%",
    );
    assertEquals(
      stylesheet.toString(),
      ".aspect-ratio\\:16\\/9{aspect-ratio:16/9}.color\\:\\#fff-\\!important{color:#fff !important}.filter\\:blur\\(10\\%\\)-sepia\\(30\\%\\){filter:blur(10%) sepia(30%)}.grid-column\\:1\\/-1{grid-column:1/-1}.width\\:33\\.3\\%{width:33.3%}",
    );

    // Trims whitespace around property name and value
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss(" display:flex  gap:[ 10px 20px ]  width:50% "),
      "display:flex gap:10px-20px width:50%",
    );
    assertEquals(
      stylesheet.toString(),
      ".display\\:flex{display:flex}.gap\\:10px-20px{gap:10px 20px}.width\\:50\\%{width:50%}",
    );

    // Ignores property if name or value is empty
    stylesheet = new UtilityStylesheet();
    assertEquals(stylesheet.ucss(" a b:1 c: :4 "), "b:1");
    assertEquals(stylesheet.toString(), ".b\\:1{b:1}");

    // Sets value if property name ends with dash-number and value is undefined
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss("bottom-0 column-gap-1 gap-2 left-2.5 row-gap-3 top-10"),
      "bottom-0 column-gap-1 gap-2 left-2.5 row-gap-3 top-10",
    );
    assertEquals(
      stylesheet.toString(),
      ".bottom-0{bottom:0rem}.column-gap-1{column-gap:0.25rem}.gap-2{gap:0.5rem}.left-2\\.5{left:0.625rem}.row-gap-3{row-gap:0.75rem}.top-10{top:2.5rem}",
    );

    // Property names can have alias
    const aliasTests: [input: string, expectedStylesheet: string][] = [[
      "m-4 m:1px",
      ".m-4{margin:1rem}.m\\:1px{margin:1px}",
    ], [
      "mb-4 mb:1px",
      ".mb-4{margin-bottom:1rem}.mb\\:1px{margin-bottom:1px}",
    ], [
      "ml-4 ml:1px",
      ".ml-4{margin-left:1rem}.ml\\:1px{margin-left:1px}",
    ], [
      "mr-4 mr:1px",
      ".mr-4{margin-right:1rem}.mr\\:1px{margin-right:1px}",
    ], [
      "mt-4 mt:1px",
      ".mt-4{margin-top:1rem}.mt\\:1px{margin-top:1px}",
    ], [
      "mx-4 mx:1px",
      ".mx-4{margin-left:1rem;margin-right:1rem}.mx\\:1px{margin-left:1px;margin-right:1px}",
    ], [
      "my-4 my:1px",
      ".my-4{margin-bottom:1rem;margin-top:1rem}.my\\:1px{margin-bottom:1px;margin-top:1px}",
    ], [
      "p-4 p:1px",
      ".p-4{padding:1rem}.p\\:1px{padding:1px}",
    ], [
      "pb-4 pb:1px",
      ".pb-4{padding-bottom:1rem}.pb\\:1px{padding-bottom:1px}",
    ], [
      "pl-4 pl:1px",
      ".pl-4{padding-left:1rem}.pl\\:1px{padding-left:1px}",
    ], [
      "pr-4 pr:1px",
      ".pr-4{padding-right:1rem}.pr\\:1px{padding-right:1px}",
    ], [
      "pt-4 pt:1px",
      ".pt-4{padding-top:1rem}.pt\\:1px{padding-top:1px}",
    ], [
      "px-4 px:1px",
      ".px-4{padding-left:1rem;padding-right:1rem}.px\\:1px{padding-left:1px;padding-right:1px}",
    ], [
      "py-4 py:1px",
      ".py-4{padding-bottom:1rem;padding-top:1rem}.py\\:1px{padding-bottom:1px;padding-top:1px}",
    ]];
    for (const [input, expectedStylesheet] of aliasTests) {
      stylesheet = new UtilityStylesheet();
      assertEquals(stylesheet.ucss(input), input);
      assertEquals(stylesheet.toString(), expectedStylesheet);
    }

    // Appends `!important` to value if short-form property ends with !
    stylesheet = new UtilityStylesheet();
    assertEquals(stylesheet.ucss("gap-0.5! m-4! px-2!"), "gap-0.5! m-4! px-2!");
    assertEquals(
      stylesheet.toString(),
      ".gap-0\\.5\\!{gap:0.125rem !important}.m-4\\!{margin:1rem !important}.px-2\\!{padding-left:0.5rem !important;padding-right:0.5rem !important}",
    );

    // Sorts class names
    stylesheet = new UtilityStylesheet();
    assertEquals(stylesheet.ucss("c-1 a:2 b:3"), "a:2 b:3 c-1");
    assertEquals(
      stylesheet.toString(),
      ".a\\:2{a:2}.b\\:3{b:3}.c-1{c:0.25rem}",
    );

    // Adds vendor prefix to certain property names or values
    const tests: [input: string, expectedStylesheet: string][] = [[
      "user-select:all",
      ".user-select\\:all{-webkit-user-select:all;user-select:all}",
    ], [
      "user-select:auto",
      ".user-select\\:auto{-webkit-user-select:auto;user-select:auto}",
    ], [
      "user-select:contain",
      ".user-select\\:contain{-webkit-user-select:contain;user-select:contain}",
    ], [
      "user-select:none",
      ".user-select\\:none{-webkit-user-select:none;user-select:none}",
    ], [
      "user-select:text",
      ".user-select\\:text{-webkit-user-select:text;user-select:text}",
    ], [
      "width:fit-content",
      ".width\\:fit-content{width:-moz-fit-content;width:fit-content}",
    ], [
      "width:max-content",
      ".width\\:max-content{width:-moz-max-content;width:max-content}",
    ], [
      "width:min-content",
      ".width\\:min-content{width:-moz-min-content;width:min-content}",
    ]];
    for (const [input, expectedStylesheet] of tests) {
      stylesheet = new UtilityStylesheet();
      assertEquals(stylesheet.ucss(input), input);
      assertEquals(stylesheet.toString(), expectedStylesheet);
    }

    // Supports states
    const statePrefixTests: [input: string, expectedStylesheet: string][] = [[
      "active:color:red active:m-8",
      ".active\\:color\\:red:active{color:red}.active\\:m-8:active{margin:2rem}",
    ], [
      "focus:color:red focus:m-8",
      ".focus\\:color\\:red:focus{color:red}.focus\\:m-8:focus{margin:2rem}",
    ], [
      "focus-visible:color:red focus-visible:m-8",
      ".focus-visible\\:color\\:red:focus-visible{color:red}.focus-visible\\:m-8:focus-visible{margin:2rem}",
    ], [
      "focus-within:color:red focus-within:m-8",
      ".focus-within\\:color\\:red:focus-within{color:red}.focus-within\\:m-8:focus-within{margin:2rem}",
    ], [
      "hover:color:red hover:m-8",
      ".hover\\:color\\:red:hover{color:red}.hover\\:m-8:hover{margin:2rem}",
    ], [
      "target:color:red target:m-8",
      ".target\\:color\\:red:target{color:red}.target\\:m-8:target{margin:2rem}",
    ], [
      "visited:color:red visited:m-8",
      ".visited\\:color\\:red:visited{color:red}.visited\\:m-8:visited{margin:2rem}",
    ]];
    for (const [input, expectedStylesheet] of statePrefixTests) {
      stylesheet = new UtilityStylesheet();
      assertEquals(stylesheet.ucss(input), input);
      assertEquals(stylesheet.toString(), expectedStylesheet);
    }

    // Supports combined states
    stylesheet = new UtilityStylesheet();
    assertEquals(
      stylesheet.ucss("hover:visited:color:red hover:visited:m-8"),
      "hover:visited:color:red hover:visited:m-8",
    );
    assertEquals(
      stylesheet.toString(),
      ".hover\\:visited\\:color\\:red:hover:visited{color:red}.hover\\:visited\\:m-8:hover:visited{margin:2rem}",
    );

    // Supports themes
    const themePrefixTests: [input: string, expectedStylesheet: string][] = [[
      "dark:color:red dark:m-8",
      "@media(prefers-color-scheme:dark){.dark\\:color\\:red{color:red}.dark\\:m-8{margin:2rem}}",
    ], [
      "light:color:red light:m-8",
      "@media(prefers-color-scheme:light){.light\\:color\\:red{color:red}.light\\:m-8{margin:2rem}}",
    ], [
      "color:black dark:color:red dark:m-8 light:color:blue light:m-4",
      ".color\\:black{color:black}@media(prefers-color-scheme:dark){.dark\\:color\\:red{color:red}.dark\\:m-8{margin:2rem}}@media(prefers-color-scheme:light){.light\\:color\\:blue{color:blue}.light\\:m-4{margin:1rem}}",
    ]];
    for (const [input, expectedStylesheet] of themePrefixTests) {
      stylesheet = new UtilityStylesheet();
      assertEquals(stylesheet.ucss(input), input);
      assertEquals(stylesheet.toString(), expectedStylesheet);
    }
  });
});
