import { css } from "@/css/css.ts";
import { assertEquals } from "@/deps.ts";

Deno.test(css.name, () => {
  const color = "red";

  const style = css`
    .item {
      color: ${color};
    }
  `;

  assertEquals(style, ".item {color: red;}");
});
