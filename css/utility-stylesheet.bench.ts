import { css } from "@/css/css.ts";
import { createU, UtilityStylesheet } from "./utility-stylesheet.ts";
import { Component, div } from "@/mod.ts";

/*
$ deno bench css/utility-stylesheet.bench.ts
cpu: Apple M1
runtime: deno 1.40.3 (aarch64-apple-darwin)

benchmark                                  time (avg)        iter/s             (min … max)       p75       p99      p995
------------------------------------------------------------------------------------------- -----------------------------
UtilityStylesheet.ucss 5000×               49.13 ms/iter          20.4   (48.99 ms … 49.69 ms) 49.18 ms 49.69 ms 49.69 ms
Create 5000 elements with utility CSS      51.68 ms/iter          19.4    (51.1 ms … 54.14 ms) 51.7 ms 54.14 ms 54.14 ms
Create 5000 elements with regular CSS     103.19 µs/iter       9,690.5  (87.42 µs … 509.71 µs) 92.04 µs 362.79 µs 372 µs
*/

Deno.bench(`${UtilityStylesheet.name}.ucss 5000×`, () => {
  const stylesheet = new UtilityStylesheet();
  for (let i = 0; i < 5000; i++) {
    stylesheet.ucss(`
      align-items:center
      background-color:gray
      display:flex
      gap:[5px 10px]
      m-4
      width:75%
      hover:background-color:blue
      dark:background-color:purple
      dark:hover:background-color:red
    `);
  }
  stylesheet.toString();
});

Deno.bench(`Create 5000 elements with utility CSS`, { baseline: true }, () => {
  const utilityStylesheet = new UtilityStylesheet();
  const u = createU(utilityStylesheet);
  const component = new Component();

  for (let i = 0; i < 5000; i++) {
    component.content.push(
      div({
        class: u`
          align-items:center
          background-color:gray
          display:flex
          gap:[5px 10px]
          m-4
          width:75%
          hover:background-color:blue
          dark:background-color:purple
          dark:hover:background-color:red
        `,
      }),
    );
  }
});

Deno.bench("Create 5000 elements with regular CSS", () => {
  const style = css`
    .item {
      align-items: center;
      background-color: gray;
      display: flex;
      gap: 5px 10px;
      margin: 1rem;
      width: 75%;
    }
    .item:hover {
      background-color: blue;
    }
    @media (prefers-color-scheme: dark) {
      .item {
        background-color: purple;
      }
      .item:hover {
        background-color: red;
      }
    }
  `;

  const component = new Component([], { style });

  for (let i = 0; i < 5000; i++) {
    component.content.push(
      div({ class: "item" }),
    );
  }
});
