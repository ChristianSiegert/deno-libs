import { assertEquals } from "../deps.ts";
import { ipv4ToNumber, isIpv4InRange } from "./ips.ts";

Deno.test(isIpv4InRange.name, () => {
  assertEquals(isIpv4InRange("12.34.55.255", ["12.34.56.0/24"]), false);
  assertEquals(isIpv4InRange("12.34.56.0", ["12.34.56.0/24"]), true);
  assertEquals(isIpv4InRange("12.34.56.78", ["12.34.56.0/24"]), true);
  assertEquals(isIpv4InRange("12.34.56.255", ["12.34.56.0/24"]), true);
  assertEquals(isIpv4InRange("12.34.57.0", ["12.34.56.0/24"]), false);
  assertEquals(isIpv4InRange("254.255.255.255", ["255.0.0.0/8"]), false);
  assertEquals(isIpv4InRange("255.0.0.0", ["255.0.0.0/8"]), true);
  assertEquals(isIpv4InRange("255.255.255.255", ["255.0.0.0/8"]), true);
  assertEquals(
    isIpv4InRange("66.249.79.63", ["66.249.71.0/27", "66.249.79.64/27"]),
    false,
  );
  assertEquals(
    isIpv4InRange("66.249.79.64", ["66.249.71.0/27", "66.249.79.64/27"]),
    true,
  );
  assertEquals(
    isIpv4InRange("66.249.79.95", ["66.249.71.0/27", "66.249.79.64/27"]),
    true,
  );
  assertEquals(
    isIpv4InRange("66.249.79.96", ["66.249.71.0/27", "66.249.79.64/27"]),
    false,
  );
});

Deno.test(ipv4ToNumber.name, () => {
  assertEquals(
    ipv4ToNumber("0.0.0.0").toString(2),
    "0",
  );
  assertEquals(
    ipv4ToNumber("127.0.0.1").toString(2),
    "1111111000000000000000000000001",
  );
  assertEquals(
    ipv4ToNumber("255.255.255.255").toString(2),
    "11111111111111111111111111111111",
  );

  assertEquals(ipv4ToNumber("").toString(2), "0");
  assertEquals(ipv4ToNumber("0").toString(2), "0");
  assertEquals(
    ipv4ToNumber("127").toString(2),
    "1111111000000000000000000000000",
  );
  assertEquals(ipv4ToNumber("a.b.c.d").toString(2), "0");
});
