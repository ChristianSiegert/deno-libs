/**
 * Fetches IP ranges of Google
 * @unstable
 */
export async function fetchGoogleIpRanges(): Promise<
  {
    creationTime: string; // ISO date
    prefixes: ({ ipv4Prefix: string } | { ipv6Prefix: string })[];
    syncToken: string; // Unix timestamp in milliseconds
  } | undefined
> {
  const url = "https://www.gstatic.com/ipranges/goog.json";
  const response = await fetch(url);
  return response.ok ? await response.json() : undefined;
}

/**
 * Fetches IP ranges of Googlebot
 * @unstable
 */
export async function fetchGooglebotIpRanges(): Promise<
  {
    creationTime: string; // ISO date
    prefixes: ({ ipv4Prefix: string } | { ipv6Prefix: string })[];
  } | undefined
> {
  const url = "https://developers.google.com/static/search/apis/ipranges/googlebot.json";
  const response = await fetch(url);
  return response.ok ? await response.json() : undefined;
}

/**
 * @param ip IPv4 in dot notation, e.g. 12.34.56.78
 * @returns IPv4 in decimal
 * @unstable
 */
export function ipv4ToNumber(ip: string): number {
  const parts = ip.split(".").map(Number);
  return (parts.at(0) || 0) * 2 ** 24 +
    (parts.at(1) || 0) * 2 ** 16 +
    (parts.at(2) || 0) * 2 ** 8 +
    (parts.at(3) || 0);
}

/**
 * @param ip IPv4 in dot notation, e.g. 12.34.56.78
 * @param ranges IPv4 ranges in CIDR notation, e.g. 12.34.56.0/24
 * @returns True if the IP is in any of the provided ranges
 * @unstable
 */
export function isIpv4InRange(ip: string, ranges: string[]): boolean {
  const ip_ = ipv4ToNumber(ip);
  return ranges.some((range) => {
    const [prefixIp, maskLength] = range.split("/");
    const mask = -1 << (32 - Number.parseInt(maskLength));
    return (ipv4ToNumber(prefixIp) & mask) === (ip_ & mask);
  });
}
