import { addFlyHeaders } from "./http.ts";
import { assertEquals } from "../deps.ts";

Deno.test(`${addFlyHeaders.name} sets response header "x-vm"`, () => {
  const tests: [
    region: string | null,
    allocId: string | null,
    expected: string,
  ][] = [
    [null, null, "undefined/undefined"],
    [null, "b996131a-5bae-215b-d0f1-2d75d1a8812b", "undefined/b996131a"],
    ["fra", null, "fra/undefined"],
    ["fra", "b996131a-5bae-215b-d0f1-2d75d1a8812b", "fra/b996131a"],
  ];

  tests.forEach(([region, allocId, expected]) => {
    if (region) {
      Deno.env.set("FLY_REGION", region);
    } else {
      Deno.env.delete("FLY_REGION");
    }

    if (allocId) {
      Deno.env.set("FLY_ALLOC_ID", allocId);
    } else {
      Deno.env.delete("FLY_ALLOC_ID");
    }

    const response = new Response();
    addFlyHeaders(response);
    assertEquals(response.headers.get("x-vm"), expected);
  });

  Deno.env.delete("FLY_ALLOC_ID");
  Deno.env.delete("FLY_REGION");
});
