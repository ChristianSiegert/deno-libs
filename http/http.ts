/**
 * Commonly used values for the `content-type` header. For content types
 * starting with `text/`, charset is set to `utf-8`.
 */
export const ContentType = {
  ApplicationJson: "application/json",
  ApplicationPdf: "application/pdf",
  ApplicationXml: "application/xml",
  ApplicationZip: "application/zip",
  AudioMpeg: "audio/mpeg",
  AudioOgg: "audio/ogg",
  AudioWebm: "audio/webm",
  ImageGif: "image/gif",
  ImageIcon: "image/x-icon",
  ImageJpeg: "image/jpeg",
  ImagePng: "image/png",
  ImageSvg: "image/svg+xml",
  TextCss: "text/css;charset=utf-8",
  TextCsv: "text/csv;charset=utf-8",
  TextHtml: "text/html;charset=utf-8",
  TextJavascript: "text/javascript;charset=utf-8",
  TextPlain: "text/plain;charset=utf-8",
  VideoMp4: "video/mp4",
  VideoMpeg: "video/mpeg",
  VideoOgg: "video/ogg",
  VideoWebm: "video/webm",
} as const;

export function addCorsHeaders(
  allowedOrigins: Set<string>,
  request: Request,
  response: Response,
): Response {
  const requestHeaders = request.headers;
  const responseHeaders = response.headers;

  const origin = requestHeaders.get("origin") ?? "";

  if (allowedOrigins.has("*")) {
    responseHeaders.set("access-control-allow-origin", "*");
  } else if (allowedOrigins.has(origin)) {
    responseHeaders.set("access-control-allow-origin", origin);
  } else {
    return response;
  }

  const acrh = requestHeaders.get("access-control-request-headers");
  if (acrh) {
    responseHeaders.set("access-control-allow-headers", acrh);
  }

  return response;
}

export function addFlyHeaders(response: Response): Response {
  const flyAllocId = Deno.env.get("FLY_ALLOC_ID")?.split("-").at(0);
  const flyRegion = Deno.env.get("FLY_REGION");

  response.headers.set("x-vm", `${flyRegion}/${flyAllocId}`);
  return response;
}
