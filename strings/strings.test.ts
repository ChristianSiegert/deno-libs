import { assertEquals } from "../deps.ts";
import { normalize } from "./strings.ts";

Deno.test(normalize.name, () => {
  const input =
    "aáàâåäãąāæbb́ḃb̧cććĉččc̊c̈c̃ċc̸çdd̂ďd̈d̃ḋḑeéèêëẽėęēff̈ḟf̸gǵg̀ğĝǧg̈g̃ġģhĥḧiíìîïījj̈kḱk̂ǩk̈k̸ķll̂l̈l̃łmm̀m̂m̌m̊m̈m̃ṁnńǹn̂ňn̊n̈ñṅņṇṋoóòôöőõøōœpp̂p̈ṗqq́rŕr̀r̂řr̈r̃sśs̀ŝšs̊s̈s̃ṡs̸şșßtt̀t̂ẗţțuúùŭûüűūvv̂v̈v̸wẃẅxẍyýỳŷẙÿỹy̎zźžż" +
    "AÁÀÂÅÄÃĄĀÆBB́ḂB̧CĆĆĈČČC̊C̈C̃ĊC̸ÇDD̂ĎD̈D̃ḊḐEÉÈÊËẼĖĘĒFF̈ḞF̸GǴG̀ĞĜǦG̈G̃ĠĢHĤḦIÍÌÎÏĪJJ̈KḰK̂ǨK̈K̸ĶLL̂L̈L̃ŁMM̀M̂M̌M̊M̈M̃ṀNŃǸN̂ŇN̊N̈ÑṄŅṆṊOÓÒÔÖŐÕØŌŒPP̂P̈ṖQQ́RŔR̀R̂ŘR̈R̃SŚS̀ŜŠS̊S̈S̃ṠS̸ŞȘẞTT̀T̂T̈ŢȚUÚÙŬÛÜŰŪVV̂V̈V̸WẂẄXẌYÝỲŶY̊ŸỸY̎ZŹŽŻ";
  const expected =
    "aaaaaaaaaabbbbccccccccccccdddddddeeeeeeeeeffffgggggggggghhhiiiiiijjkkkkkkklllllmmmmmmmmnnnnnnnnnnnnooooooooooppppqqrrrrrrrsssssssssssssttttttuuuuuuuuvvvvwwwxxyyyyyyyyzzzz" +
    "aaaaaaaaaabbbbccccccccccccdddddddeeeeeeeeeffffgggggggggghhhiiiiiijjkkkkkkklllllmmmmmmmmnnnnnnnnnnnnooooooooooppppqqrrrrrrrsssssssssssssttttttuuuuuuuuvvvvwwwxxyyyyyyyyzzzz";

  assertEquals(normalize(input), expected);

  // Assert that all instances of character are replaced, not only first match
  assertEquals(normalize(`${input}${input}`), `${expected}${expected}`);
});
