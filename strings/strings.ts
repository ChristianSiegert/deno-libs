/**
 * Returns string where characters are lowercased and replaced with a latinized
 * version, e.g. ä with a and É with e. Useful for sorting by normalized value.
 */
export function normalize(value: string): string {
  return value
    .toLowerCase()
    .normalize("NFKD")
    .replace(/[\u0300-\u036f]/g, "")
    .replaceAll("æ", "a")
    .replaceAll("ł", "l")
    .replaceAll("ø", "o")
    .replaceAll("œ", "o")
    .replaceAll("ß", "s");
}
