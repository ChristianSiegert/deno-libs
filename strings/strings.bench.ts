import { normalize } from "./strings.ts";

Deno.bench(normalize.name, () => {
  const input = "aäàáâæãåābcçćčdeéèêëėfghiîïíīìjklmnñńoöôòóõœøōpqrsßśštuüûùúūvwxyÿz" +
    "AÄÀÁÂÆÃÅĀBCÇĆČDEÉÈÊËĖFGHIÎÏÍĪÌJKLMNÑŃOÖÔÒÓÕŒØŌPQRSẞŚŠTUÜÛÙÚŪVWXYŸZ";
  normalize(input);
});
