import { assertEquals } from "../deps.ts";
import { emailPattern } from "./validator.ts";

Deno.test("emailPattern", () => {
  const p = emailPattern;

  const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  assertEquals(p.test("a@example.com"), true);
  assertEquals(p.test("a.b@example.com"), true);
  assertEquals(p.test("a-b@example.com"), true);
  assertEquals(p.test("a.b+tag@example.com"), true);
  assertEquals(p.test("a-b+tag@example.com"), true);
  assertEquals(p.test("a.a.a.b-b-b-c+c+c+@a-a-a.b-b-b.c-c-c.d"), true);
  assertEquals(
    p.test(`${chars}.${chars}-${chars}+${chars}@${chars}.${chars}`),
    true,
  );

  // HTML 5 email pattern allows these addresses
  assertEquals(p.test("a@b"), true);
  assertEquals(p.test("a..b@example.com"), true);
  assertEquals(p.test("a++b@example.com"), true);
  assertEquals(p.test("a#b@example.com"), true);
  assertEquals(p.test("a#@example.com"), true);
  assertEquals(p.test("#a@example.com"), true);
  assertEquals(p.test("a.@example.com"), true);
  assertEquals(p.test(".a@example.com"), true);
  assertEquals(p.test("1@2"), true);

  // Invalid
  assertEquals(p.test("a@b@"), false);
  assertEquals(p.test("@a@b"), false);
  assertEquals(p.test("a"), false);
  assertEquals(p.test("b.com"), false);
  assertEquals(p.test("a@"), false);
  assertEquals(p.test("@b.com"), false);
  assertEquals(p.test("a@b+c.com"), false);
  assertEquals(p.test("a b@c.com"), false);
  assertEquals(p.test("a@b.com "), false);
  assertEquals(p.test(" a@b.com"), false);
});
