import { type Validator } from "./validator.ts";

/** @unstable */
export class FormField {
  /** Validation message to show to user. Setting a message means the form field
   *  did not pass validation. */
  message = "";

  /** Name of the form field. */
  name: string;

  /** Wether to remove leading and trailing whitespace from string values. */
  trim: boolean;

  /** Functions to validate the form field. */
  validators: Validator[];

  #values: (string | File)[] = [];

  /** Getter that returns all values that are a file. */
  get files(): File[] {
    return this.#values
      .filter<File>((value): value is File => value instanceof File);
  }

  /** Getter that returns all values that are a string. */
  get strings(): string[] {
    return this.#values
      .filter<string>((value): value is string => typeof value === "string");
  }

  /** Getter: Gets the first value and returns it as string. If not present, returns the empty string.
   *
   *  Setter: Replaces values array with new array that only contains the given value. */
  get value(): string {
    return this.#values.length ? String(this.#values[0]) : "";
  }

  set value(value: string | File) {
    this.values = [value];
  }

  get values(): (string | File)[] {
    return this.#values;
  }

  set values(values: (string | File)[]) {
    this.#values = this.trim
      ? values.map((value) => typeof value === "string" ? value.trim() : value)
      : values;
  }

  constructor(options: {
    name: string;
    trim?: boolean;
    validators?: Validator[];
    value?: string | File;
    values?: (string | File)[];
  }) {
    this.name = options.name;
    this.trim = options.trim ?? true;
    this.validators = options.validators ?? [];

    if (options.value) {
      this.value = options.value;
    } else if (options.values) {
      this.values = options.values;
    }
  }

  /** Validates `this.values` with the configured validators. Resolves to true
   *  if it passes validation. */
  async validate(): Promise<boolean> {
    this.message = "";

    for (const validator of this.validators) {
      if (!await validator.fn(this)) {
        this.message = validator.message(this);
        return false;
      }
    }
    return true;
  }
}
