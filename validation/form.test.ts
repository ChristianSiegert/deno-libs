import { Form, FormField } from "./mod.ts";
import { assertEquals } from "../deps.ts";

Deno.test(Form.name, async (t) => {
  await t.step("validate() validates all form fields", async () => {
    class TestForm extends Form {
      field1 = new FormField({
        name: "f1",
        validators: [{ fn: ({ value }) => !!value, message: () => "required" }],
      });
      field2 = new FormField({
        name: "f2",
        validators: [{ fn: ({ value }) => !!value, message: () => "required" }],
      });
    }
    const form = new TestForm();
    assertEquals(form.field1.message, "");
    assertEquals(form.field2.message, "");

    assertEquals(await form.validate(), false);
    assertEquals(form.field1.message, "required");
    assertEquals(form.field2.message, "required");
  });

  await t.step("Empty form passes validation", async () => {
    class TestForm extends Form {}
    assertEquals(await new TestForm().validate(), true);
  });

  await t.step(
    "Form field validator can access other form fields",
    async () => {
      class TestForm extends Form {
        field1 = new FormField({ name: "f1" });
        field2 = new FormField({
          name: "f2",
          validators: [{
            fn: (field) => this.field1.value.length + field.value.length > 4,
            message: () => "combined length must be greater than 4",
          }],
        });
      }
      const form = new TestForm();
      assertEquals(await form.validate(), false);
      form.field1.value = "aa";
      form.field2.value = "bbb";
      assertEquals(await form.validate(), true);
    },
  );

  await t.step("Accepts generic context", async () => {
    type Context = { t: (translationKey: string) => string };

    class TestForm extends Form<Context> {
      field1 = new FormField({
        name: "f1",
        validators: [{
          fn: () => false,
          message: () => this.context.t("invalid"),
        }],
      });
    }

    const context: Context = { t: (translationKey) => translationKey + "!!" };
    const form = new TestForm(context);
    form.field1.value = "always invalid";
    assertEquals(await form.validate(), false);
    assertEquals(form.field1.message, "invalid!!");
  });

  await t.step("valuesFrom()", () => {
    class TestForm extends Form {
      field1 = new FormField({ name: "f1", trim: true });
      field2 = new FormField({ name: "f2", trim: false });
    }
    const form = new TestForm();

    const formData = new FormData();
    formData.append("f1", " a ");
    formData.append("f1", " b ");
    formData.append("f1", " ");
    formData.append("f2", " c ");
    formData.append("f2", " d ");
    formData.append("f2", " ");
    form.valuesFrom(formData);
    assertEquals(form.field1.value, "a");
    assertEquals(form.field1.values, ["a", "b", ""]);
    assertEquals(form.field2.value, " c ");
    assertEquals(form.field2.values, [" c ", " d ", " "]);

    const searchParams = new URLSearchParams();
    searchParams.append("f1", " e ");
    searchParams.append("f1", " f ");
    searchParams.append("f1", " ");
    searchParams.append("f2", " g ");
    searchParams.append("f2", " h ");
    searchParams.append("f2", " ");
    form.valuesFrom(searchParams);
    assertEquals(form.field1.value, "e");
    assertEquals(form.field1.values, ["e", "f", ""]);
    assertEquals(form.field2.value, " g ");
    assertEquals(form.field2.values, [" g ", " h ", " "]);
  });

  await t.step("clear() clears form field values", () => {
    class TestForm extends Form {
      field1 = new FormField({ name: "f1", value: "a" });
      field2 = new FormField({ name: "f2", values: ["b", "c"] });
      noFormField = "n";
    }
    const form = new TestForm();
    form.clear();
    assertEquals(form.field1.value, "");
    assertEquals(form.field1.values, []);
    assertEquals(form.field2.value, "");
    assertEquals(form.field2.values, []);
    assertEquals(form.noFormField, "n");
  });
});
