import { assertEquals } from "../deps.ts";
import { FormField } from "./mod.ts";

Deno.test(FormField.name, async (t) => {
  await t.step("Defaults", () => {
    const field = new FormField({ name: "field1" });
    assertEquals(field.message, "");
    assertEquals(field.name, "field1");
    assertEquals(field.trim, true);
    assertEquals(field.validators, []);
    assertEquals(field.value, "");
    assertEquals(field.values, []);
  });

  await t.step(
    "If both `value` and `values` are provided in constructor, `value` is used",
    () => {
      const field = new FormField({
        name: "field1",
        value: "a",
        values: ["b"],
      });
      assertEquals(field.value, "a");
      assertEquals(field.values, ["a"]);
    },
  );

  await t.step(
    "If trim is enabled, trims whitespace from string values",
    () => {
      const file = new File(["hello"], "abc.txt", { type: "text/plain" });

      // Setting `value`
      let field = new FormField({ name: "field", trim: true, value: " a " });
      assertEquals(field.value, "a");
      assertEquals(field.values, ["a"]);
      field.value = " b ";
      assertEquals(field.value, "b");
      assertEquals(field.values, ["b"]);
      field.value = " ";
      assertEquals(field.value, "");
      assertEquals(field.values, [""]);
      field.value = "";
      assertEquals(field.value, "");
      assertEquals(field.values, [""]);
      field.value = file;
      assertEquals(field.value, "[object File]");
      assertEquals(field.values, [file]);

      field = new FormField({ name: "field", trim: false, value: " a " });
      assertEquals(field.value, " a ");
      assertEquals(field.values, [" a "]);
      field.value = " b ";
      assertEquals(field.value, " b ");
      assertEquals(field.values, [" b "]);
      field.value = " ";
      assertEquals(field.value, " ");
      assertEquals(field.values, [" "]);
      field.value = "";
      assertEquals(field.value, "");
      assertEquals(field.values, [""]);
      field.value = file;
      assertEquals(field.value, "[object File]");
      assertEquals(field.values, [file]);

      // Setting `values`
      field = new FormField({
        name: "field",
        trim: true,
        values: [" a ", " b ", " ", "", file],
      });
      assertEquals(field.values, ["a", "b", "", "", file]);
      assertEquals(field.value, "a");
      field.values = [" c ", " d ", " ", "", file];
      assertEquals(field.values, ["c", "d", "", "", file]);
      assertEquals(field.value, "c");
      field.values = [];
      assertEquals(field.values, []);
      assertEquals(field.value, "");

      field = new FormField({
        name: "field",
        trim: false,
        values: [" a ", " b ", " ", "", file],
      });
      assertEquals(field.values, [" a ", " b ", " ", "", file]);
      assertEquals(field.value, " a ");
      field.values = [" c ", " d ", " ", "", file];
      assertEquals(field.values, [" c ", " d ", " ", "", file]);
      assertEquals(field.value, " c ");
      field.values = [];
      assertEquals(field.values, []);
      assertEquals(field.value, "");
    },
  );

  await t.step("Getters `files` and `strings`", () => {
    const file = new File(["hello"], "abc.txt", { type: "text/plain" });
    const field = new FormField({ name: "field", values: ["a", file, "b"] });
    assertEquals(field.files, [file]);
    assertEquals(field.strings, ["a", "b"]);
  });

  await t.step("Form field without validators passes validation", async () => {
    const field = new FormField({ name: "field" });
    assertEquals(await field.validate(), true);
  });

  await t.step("Validators are called in order", async () => {
    const callOrder: number[] = [];

    const field = new FormField({
      name: "field",
      validators: [{
        fn: (field) => {
          callOrder.push(1);
          return field.value.length > 2;
        },
        message: () => "too short",
      }, {
        fn: (field) => {
          callOrder.push(2);
          return field.value.length < 10;
        },
        message: () => "too long",
      }],
    });

    field.value = "hello";
    await field.validate();
    assertEquals(callOrder, [1, 2]);
  });

  await t.step("validate() clears previous validation message", async () => {
    const field = new FormField({
      name: "field",
      validators: [{
        fn: (field) => !!field.value,
        message: () => "required",
      }],
    });
    assertEquals(await field.validate(), false);
    assertEquals(field.message, "required");

    field.value = "hello";
    assertEquals(await field.validate(), true);
    assertEquals(field.message, "");
  });
});
