import { FormField } from "./form-field.ts";

/**
 * Pattern of valid e-mail address according to HTML 5 standard
 * <https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address>
 * @unstable Allows uncommon/invalid email addresses. See unit test.
 */
export const emailPattern =
  /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

/** @unstable */
export type Validator = {
  fn: (field: FormField) => boolean | Promise<boolean>;
  message: (field: FormField) => string;
};
