import { FormField } from "./form-field.ts";

/**
 * Form validates form fields. Create a new class that extends this class, and
 * add a class member of type `FormField` for each form field. In your request
 * handler, call `form.valuesFrom()`, then `await form.validate()`.
 * @example
 * ```ts
 * class RegistrationForm extends Form {
 *   emailAddress = new FormField({
 *     name: "emailAddress",
 *     validators: [{
 *       fn: ({ value }) => value.endsWith("@example.com"),
 *       message: () => "Must end with example.com",
 *     }],
 *   });
 *   password = new FormField({
 *     name: "password",
 *     validators: [{
 *       fn: ({ value }) => value.length >= 8,
 *       message: () => "Must be at least 8 characters long",
 *     }],
 *   });
 * }
 *
 * async function handleRequest(request: Request) {
 *   const form = new RegistrationForm();
 *   if (request.method === "POST") {
 *     const formData = await request.formData();
 *     form.valuesFrom(formData);
 *     if (await form.validate()) {
 *       // Create account
 *     }
 *   }
 * }
 * ```
 * @unstable
 */
export class Form<Context = void> {
  context: Context;

  /** isValid caches the last result of validate(). Defaults to false. */
  isValid = false;

  constructor(context: Context) {
    this.context = context;
  }

  /** Removes all values from the form fields. */
  clear(): void {
    this.#getFields().forEach((field) => field.values = []);
  }

  /**
   * Validates all form fields. Form fields that have no validator pass as valid
   * by default.
   * @returns True if all form fields passed validation. Otherwise false.
   */
  async validate(): Promise<boolean> {
    const fields = this.#getFields();
    this.isValid = (await Promise.all(fields.map((field) => field.validate())))
      .every((fieldIsValid) => fieldIsValid);
    return this.isValid;
  }

  /** Populate form fields. */
  valuesFrom(data: FormData | URLSearchParams): void {
    this.#getFields().forEach((field) => {
      field.values = data.getAll(field.name);
    });
  }

  #getFields(): FormField[] {
    return Object.values(this).filter((field) => field instanceof FormField);
  }
}
