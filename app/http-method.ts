export const HttpMethod = {
  CONNECT: 1 << 0,
  DELETE: 1 << 1,
  GET: 1 << 2,
  HEAD: 1 << 3,
  OPTIONS: 1 << 4,
  PATCH: 1 << 5,
  POST: 1 << 6,
  PUT: 1 << 7,
  TRACE: 1 << 8,

  [1 << 0]: "CONNECT",
  [1 << 1]: "DELETE",
  [1 << 2]: "GET",
  [1 << 3]: "HEAD",
  [1 << 4]: "OPTIONS",
  [1 << 5]: "PATCH",
  [1 << 6]: "POST",
  [1 << 7]: "PUT",
  [1 << 8]: "TRACE",

  fromString: (method: string): number => {
    const value = (HttpMethod as Record<string, unknown>)[method];
    return typeof value === "number" ? value : 0;
  },
  toString: (method: number): string => {
    const name = HttpMethod[method];
    return typeof name === "string" ? name : "";
  },
} as const;
