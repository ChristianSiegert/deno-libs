import { App } from "./app.ts";
import { delay } from "../deps.ts";
import { HttpMethod } from "./http-method.ts";

await benchDenoServe();
await delay(100);
await benchAppDenoServe();

async function benchDenoServe() {
  const abortController = new AbortController();
  const server = Deno.serve({
    hostname: "localhost",
    onListen: () => {},
    port: 8080,
    signal: abortController.signal,
  }, () => new Response("{ hello: 'world' }"));

  try {
    const iterations = 50000;
    const start = Date.now();
    for (let i = 0; i < iterations; i++) {
      const response = await fetch("http://localhost:8080/");
      await response.text();
    }
    const duration = Date.now() - start;
    console.log(
      `Deno serve(): ${iterations} requests in ${duration} ms, ${
        Math.floor(iterations / duration * 1000)
      } requests per second`,
    );
  } finally {
    abortController.abort();
    await server.finished;
  }
}

async function benchAppDenoServe() {
  const app = new App({
    createContext: (baseContext) => baseContext,
    routes: {
      about: {
        handler: () => new Response("{ hello: 'world' }"),
        methods: HttpMethod.GET,
        pattern: new URLPattern({ pathname: "/" }),
        url: () => "/",
      },
    },
  });

  const abortController = new AbortController();
  const server = Deno.serve({
    hostname: "localhost",
    onListen: () => {},
    port: 8081,
    signal: abortController.signal,
  }, app.handler);

  try {
    const iterations = 50000;
    const start = Date.now();
    for (let i = 0; i < iterations; i++) {
      const response = await fetch("http://localhost:8081/");
      await response.text();
    }
    const duration = Date.now() - start;
    console.log(
      `AppDenoServe: ${iterations} requests in ${duration} ms, ${
        Math.floor(iterations / duration * 1000)
      } requests per second`,
    );
  } finally {
    abortController.abort();
    await server.finished;
  }
}

Deno.bench(
  "Create 100 route patterns of type string",
  { group: "Create route pattern", baseline: true },
  () => {
    for (let i = 0; i < 100; i++) {
      const _url = new URL("/", "http://example.com");
      const _pattern = "/";
    }
  },
);

Deno.bench(
  "Create 100 route patterns of type URLPattern",
  { group: "Create route pattern" },
  () => {
    for (let i = 0; i < 100; i++) {
      const _url = "http://example.com/";
      const _pattern = new URLPattern({ pathname: "/" });
    }
  },
);

Deno.bench(
  "Compare 100 route patterns of type string",
  { group: "Compare route pattern", baseline: true },
  () => {
    const url = new URL("/", "http://example.com");
    const pattern = "/";

    for (let i = 0; i < 100; i++) {
      const _result = url.pathname === pattern;
    }
  },
);

Deno.bench(
  "Compare 100 route patterns of type URLPattern",
  { group: "Compare route pattern" },
  () => {
    const url = "http://example.com/";
    const pattern = new URLPattern({ pathname: "/" });

    for (let i = 0; i < 100; i++) {
      const _result = pattern.exec(url);
    }
  },
);
