import { App, type BaseContext, type Route, type Routes, typeRoutes } from "./app.ts";
import { assertEquals } from "../deps.ts";
import { HttpMethod } from "./http-method.ts";

const { GET, POST } = HttpMethod;

async function withServer<T extends BaseContext>(options: {
  app: App<T>;
  port: number;
  test: () => Promise<void>;
}) {
  const abortController = new AbortController();
  const server = Deno.serve({
    hostname: "localhost",
    onListen: () => {},
    port: options.port,
    signal: abortController.signal,
  }, options.app.handler);

  try {
    await options.test();
  } finally {
    abortController.abort();
    await server.finished;
  }
}

Deno.test(`${App.name}: Returns 200 OK if route is found`, async () => {
  type Context = BaseContext & { luckyNumber: number };

  const app = new App<Context>({
    createContext: (baseContext) => ({
      ...baseContext,
      luckyNumber: Math.floor(Math.random() * 100),
    }),
    routes: typeRoutes<Context>()({
      about: {
        handler: (_context: Context) => new Response("About page"),
        methods: GET,
        pattern: "/about",
        url: () => "/about",
      },
      profile: {
        handler: (context: Context) => {
          const username = context.urlPatternResult?.pathname.groups.username;
          return new Response(`Profile of ${username}`);
        },
        methods: GET,
        pattern: new URLPattern({ pathname: "/users/:username" }),
        url: (username: string) => `/users/${username}`,
      },
    }),
  });

  await withServer({
    app,
    port: 8765,
    test: async () => {
      let response = await fetch("http://localhost:8765/about");
      assertEquals(response.status, 200);
      assertEquals(await response.text(), "About page");

      response = await fetch("http://localhost:8765/users/JohnDoe");
      assertEquals(response.status, 200);
      assertEquals(await response.text(), "Profile of JohnDoe");
    },
  });
});

Deno.test(`${App.name}: Returns 404 Not Found if route is not found`, async () => {
  const app = new App({
    createContext: (baseContext) => baseContext,
    routes: typeRoutes()({
      route1: {
        handler: ({ request }) => new Response(`${request.method} Route 1`),
        methods: GET,
        pattern: "/route1",
        url: () => "/route1",
      },
      route2: {
        handler: ({ request }) => new Response(`${request.method} Route 2`),
        methods: GET,
        pattern: new URLPattern({ pathname: "/route2" }),
        url: () => "/route2",
      },
    }),
  });

  const httpMethods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"];

  const tests: {
    url: string;
    method: string;
    expectedStatus: number;
    expectedStatusText: string;
    expectedText: string;
    expectedContentType: string;
  }[] = httpMethods.map((method) => ({
    url: "http://localhost:8764/path-does-not-exist",
    method,
    expectedStatus: 404,
    expectedStatusText: "Not Found",
    expectedText: method === "HEAD" ? "" : "Not Found",
    expectedContentType: "text/plain;charset=utf-8",
  }));

  await withServer({
    app,
    port: 8764,
    test: async () => {
      for (const test of tests) {
        const response = await fetch(test.url, { method: test.method });
        assertEquals(response.status, test.expectedStatus);
        assertEquals(response.statusText, test.expectedStatusText);
        assertEquals(await response.text(), test.expectedText);
        assertEquals(
          response.headers.get("content-type"),
          test.expectedContentType,
        );
      }
    },
  });
});

Deno.test(`${App.name}: Returns 405 Method Not Allowed if route is found but HTTP method is not allowed`, async () => {
  const app = new App({
    createContext: (baseContext) => baseContext,
    routes: typeRoutes()({
      route1: {
        handler: ({ request }) => new Response(`${request.method} Route 1`),
        methods: GET | POST,
        pattern: "/route1",
        url: () => "/route1",
      },
      route2: {
        handler: ({ request }) => new Response(`${request.method} Route 2`),
        methods: POST,
        pattern: "/route2",
        url: () => "/route2",
      },
      route3: {
        handler: ({ request }) => new Response(`${request.method} Route 3`),
        methods: GET | POST,
        pattern: new URLPattern({ pathname: "/route3" }),
        url: () => "/route3",
      },
      route4: {
        handler: ({ request }) => new Response(`${request.method} Route 4`),
        methods: POST,
        pattern: new URLPattern({ pathname: "/route4" }),
        url: () => "/route4",
      },
    }),
  });

  const tests: {
    url: string;
    method: string;
    expectedAllowHeader: string | null;
    expectedStatus: number;
    expectedStatusText: string;
    expectedText: string;
  }[] = [{
    url: "http://localhost:8763/route1",
    method: "GET",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "GET Route 1",
  }, {
    url: "http://localhost:8763/route1",
    method: "HEAD",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "",
  }, {
    url: "http://localhost:8763/route1",
    method: "POST",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "POST Route 1",
  }, {
    url: "http://localhost:8763/route1",
    method: "DELETE",
    expectedAllowHeader: "GET,POST",
    expectedStatus: 405,
    expectedStatusText: "Method Not Allowed",
    expectedText: "HTTP Method Not Allowed",
  }, {
    url: "http://localhost:8763/route2",
    method: "HEAD",
    expectedAllowHeader: "POST",
    expectedStatus: 405,
    expectedStatusText: "Method Not Allowed",
    expectedText: "",
  }, {
    url: "http://localhost:8763/route3",
    method: "GET",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "GET Route 3",
  }, {
    url: "http://localhost:8763/route3",
    method: "HEAD",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "",
  }, {
    url: "http://localhost:8763/route3",
    method: "POST",
    expectedAllowHeader: null,
    expectedStatus: 200,
    expectedStatusText: "OK",
    expectedText: "POST Route 3",
  }, {
    url: "http://localhost:8763/route3",
    method: "DELETE",
    expectedAllowHeader: "GET,POST",
    expectedStatus: 405,
    expectedStatusText: "Method Not Allowed",
    expectedText: "HTTP Method Not Allowed",
  }, {
    url: "http://localhost:8763/route4",
    method: "HEAD",
    expectedAllowHeader: "POST",
    expectedStatus: 405,
    expectedStatusText: "Method Not Allowed",
    expectedText: "",
  }];

  await withServer({
    app,
    port: 8763,
    test: async () => {
      for (const test of tests) {
        const response = await fetch(test.url, { method: test.method });
        assertEquals(response.status, test.expectedStatus);
        assertEquals(response.statusText, test.expectedStatusText);
        assertEquals(response.headers.get("allow"), test.expectedAllowHeader);
        assertEquals(await response.text(), test.expectedText);
      }
    },
  });
});

Deno.test(`${App.name}: Returns 500 Internal Server Error if uncaught exception occurs in handler`, async () => {
  const app = new App({
    createContext: (baseContext) => baseContext,
    routes: typeRoutes()({
      route1: {
        handler: (_context) => {
          throw new Error();
        },
        methods: GET,
        pattern: "/route1",
        url: () => "/route1",
      },
    }),
  });

  const httpMethods = ["GET", "HEAD"];

  const tests: {
    url: string;
    method: string;
    expectedStatus: number;
    expectedStatusText: string;
    expectedText: string;
    expectedContentType: string;
  }[] = httpMethods.map((method) => ({
    url: "http://localhost:8755/route1",
    method,
    expectedStatus: 500,
    expectedStatusText: "Internal Server Error",
    expectedText: method === "HEAD" ? "" : "Internal Server Error",
    expectedContentType: "text/plain;charset=utf-8",
  }));

  await withServer({
    app,
    port: 8755,
    test: async () => {
      for (const test of tests) {
        const response = await fetch(test.url, { method: test.method });
        assertEquals(response.status, test.expectedStatus);
        assertEquals(response.statusText, test.expectedStatusText);
        assertEquals(await response.text(), test.expectedText);
        assertEquals(
          response.headers.get("content-type"),
          test.expectedContentType,
        );
      }
    },
  });
});

Deno.test("App: Add content type charset if needed", async () => {
  const tests: {
    routeContentType: string | null;
    expectedContentType: string;
  }[] = [{
    routeContentType: null,
    expectedContentType: "text/html;charset=utf-8",
  }, {
    routeContentType: "",
    expectedContentType: "text/html;charset=utf-8",
  }, {
    routeContentType: "text/css",
    expectedContentType: "text/css;charset=utf-8",
  }, {
    routeContentType: "text/html",
    expectedContentType: "text/html;charset=utf-8",
  }, {
    routeContentType: "text/javascript",
    expectedContentType: "text/javascript;charset=utf-8",
  }, {
    routeContentType: "text/a",
    expectedContentType: "text/a;charset=utf-8",
  }, {
    routeContentType: "text/b;",
    expectedContentType: "text/b;charset=utf-8",
  }, {
    routeContentType: "text/c; ",
    expectedContentType: "text/c;charset=utf-8",
  }, {
    routeContentType: "text/d; charset=utf-7",
    expectedContentType: "text/d;charset=utf-7",
  }, {
    routeContentType: "image/jpeg",
    expectedContentType: "image/jpeg",
  }];

  const routes: Routes<BaseContext> = {};

  const app = new App({
    createContext: (baseContext) => baseContext,
    routes,
  });
  app.enableAddContentTypeCharset = true;

  await withServer({
    app,
    port: 8762,
    test: async () => {
      let i = 0;
      for (const test of tests) {
        const route: Route<BaseContext> = {
          handler: (_context) =>
            new Response("", {
              headers: {
                ...test.routeContentType !== null ? { "content-type": test.routeContentType } : {},
              },
            }),
          methods: GET,
          pattern: new URLPattern({ pathname: `/route-${i}` }),
          url: () => `/route-${i}`,
        };
        routes[i] = route;

        const url = "http://localhost:8762" + route.url();
        const response = await fetch(url);
        await response.text();
        assertEquals(response.status, 200);
        assertEquals(
          response.headers.get("content-type"),
          test.expectedContentType,
        );
        i += 1;
      }
    },
  });
});

Deno.test("App: Serving files from public directory", async (t) => {
  await t.step("Is turned off by default", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        publicDir: ".",
      }),
      port: 8761,
      test: async () => {
        const response = await fetch("http://localhost:8761/README.md");
        assertEquals(await response.text(), "Not Found");
        assertEquals(response.status, 404);
      },
    });
  });

  await t.step("Is turned on, file is found", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        enablePublicDir: true,
        publicDir: ".",
      }),
      port: 8760,
      test: async () => {
        const response = await fetch("http://localhost:8760/README.md");
        assertEquals((await response.text()).startsWith("# deno-libs"), true);
        assertEquals(response.status, 200);
      },
    });
  });

  await t.step("Is turned on, file is not found", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        enablePublicDir: true,
        publicDir: ".",
      }),
      port: 8759,
      test: async () => {
        const response = await fetch("http://localhost:8759/NOTEXIST.md");
        assertEquals(await response.text(), "Not Found");
        assertEquals(response.status, 404);
      },
    });
  });

  await t.step("Is turned on, file is directory", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        enablePublicDir: true,
        publicDir: ".",
      }),
      port: 8758,
      test: async () => {
        const response = await fetch("http://localhost:8758/");
        assertEquals(await response.text(), "Not Found");
        assertEquals(response.status, 404);
      },
    });
  });

  await t.step("Is turned on, route preceeds file with same name", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        enablePublicDir: true,
        publicDir: ".",
        routes: typeRoutes()({
          readme: {
            handler: () => new Response("route content"),
            methods: GET,
            pattern: new URLPattern({ pathname: "/README.md" }),
            url: () => "README.md",
          },
        }),
      }),
      port: 8757,
      test: async () => {
        const response = await fetch("http://localhost:8757/README.md");
        assertEquals(await response.text(), "route content");
        assertEquals(response.status, 200);
      },
    });
  });

  await t.step("Sets content-type header", async () => {
    await withServer({
      app: new App({
        createContext: (baseContext) => baseContext,
        enablePublicDir: true,
        publicDir: "temp",
      }),
      port: 8756,
      test: async () => {
        try {
          await Deno.mkdir("temp");

          const tests: [extension: string, expectedContentType: string][] = [
            ["css", "text/css; charset=UTF-8"],
            ["csv", "text/csv; charset=UTF-8"],
            ["htm", "text/html; charset=UTF-8"],
            ["html", "text/html; charset=UTF-8"],
            ["js", "application/javascript; charset=UTF-8"],
            ["txt", "text/plain; charset=UTF-8"],

            ["jpeg", "image/jpeg"],
            ["jpg", "image/jpeg"],
            ["png", "image/png"],
          ];

          for (const [extension, expectedContentType] of tests) {
            await Deno.writeTextFile(`temp/test.${extension}`, "");
            const response = await fetch(
              `http://localhost:8756/test.${extension}`,
            );
            await response.body?.cancel();
            assertEquals(response.status, 200);
            assertEquals(
              response.headers.get("content-type"),
              expectedContentType,
            );
          }
        } finally {
          await Deno.remove("temp", { recursive: true });
        }
      },
    });
  });
});
