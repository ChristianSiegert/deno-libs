import { assertEquals } from "../deps.ts";
import { HttpMethod } from "./http-method.ts";

Deno.test("HttpMethod", () => {
  assertEquals(HttpMethod.fromString("CONNECT"), HttpMethod.CONNECT);
  assertEquals(HttpMethod.fromString("DELETE"), HttpMethod.DELETE);
  assertEquals(HttpMethod.fromString("GET"), HttpMethod.GET);
  assertEquals(HttpMethod.fromString("HEAD"), HttpMethod.HEAD);
  assertEquals(HttpMethod.fromString("OPTIONS"), HttpMethod.OPTIONS);
  assertEquals(HttpMethod.fromString("PATCH"), HttpMethod.PATCH);
  assertEquals(HttpMethod.fromString("POST"), HttpMethod.POST);
  assertEquals(HttpMethod.fromString("PUT"), HttpMethod.PUT);
  assertEquals(HttpMethod.fromString("TRACE"), HttpMethod.TRACE);
  assertEquals(HttpMethod.fromString("fromString"), 0);
  assertEquals(HttpMethod.fromString("toString"), 0);
  assertEquals(HttpMethod.fromString(""), 0);

  assertEquals(HttpMethod.toString(HttpMethod.CONNECT), "CONNECT");
  assertEquals(HttpMethod.toString(HttpMethod.DELETE), "DELETE");
  assertEquals(HttpMethod.toString(HttpMethod.GET), "GET");
  assertEquals(HttpMethod.toString(HttpMethod.HEAD), "HEAD");
  assertEquals(HttpMethod.toString(HttpMethod.OPTIONS), "OPTIONS");
  assertEquals(HttpMethod.toString(HttpMethod.PATCH), "PATCH");
  assertEquals(HttpMethod.toString(HttpMethod.POST), "POST");
  assertEquals(HttpMethod.toString(HttpMethod.PUT), "PUT");
  assertEquals(HttpMethod.toString(HttpMethod.TRACE), "TRACE");
  assertEquals(HttpMethod.toString(123), "");
});
