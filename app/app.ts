import { contentType as getContentType } from "../deps.ts";
import { ContentType } from "../http/http.ts";
import { HttpMethod } from "./http-method.ts";
import { retry } from "../retry/retry.ts";

export type BaseContext = {
  /** Connection info. */
  connInfo: Deno.ServeHandlerInfo;

  /** Request. */
  request: Request;

  /** Request URL. */
  url: URL;

  /** Is set if the route pattern is of type URLPattern and matched the request URL. */
  urlPatternResult?: URLPatternResult;
};

export type Handler<Context extends BaseContext> = (
  context: Context,
) => Response | Promise<Response>;

export type Route<Context extends BaseContext, RouteMeta = undefined> = {
  /** Returns a response. */
  handler: Handler<Context>;

  /** Custom information about this route. Useful for middleware to perform
   *  additional work, e.g. to check authentication if a “requires login”
   *  property is set to true. */
  meta?: RouteMeta;

  /** Allowed HTTP methods for this route. See `HttpMethod`. To specify more than
   *  one, use the bitwise OR operator, e.g. `HttpMethod.GET | HttpMethod.POST`. */
  methods: number;

  /** Pattern that must match the request URL for this route to be considered a match. If pattern is
   *  of type string, the string is only compared to the request URL path. */
  pattern: URLPattern | string;

  /** Returns a URL for this route based on the given arguments. */
  // deno-lint-ignore no-explicit-any
  url: (...args: any[]) => string;
};

export type Routes<Context extends BaseContext, RouteMeta = undefined> = {
  [routeName: string]: Route<Context, RouteMeta>;
};

/** Returns a function that returns the given `routes` object unchanged but now
 *  the editor understands the returned `routes` object’s properties and enables
 *  autocomplete for it. */
export function typeRoutes<
  Context extends BaseContext,
  RouteMeta = undefined,
>(): <R extends Routes<Context, RouteMeta>>(routes: R) => R {
  return (routes) => routes;
}

/**
 * App is the entry-point of the app. It serves the provided routes with the
 * stdlib serve() function. If no route matches the request URL and serving
 * files from the public directory is enabled, it serves the file that matches
 * the request URL path. If nothing matches, handlerNotFound is called.
 */
export class App<Context extends BaseContext, RouteMeta = undefined> {
  /**
   * When enabled, the server adds `charset=utf-8` to `content-type` headers
   * that begin with `text/` and have no charset. If the content type is the
   * string `text/plain;charset=UTF-8`, it is replaced with
   * `text/html;charset=utf-8` so `new Response("")` automatically serves HTML.
   * @unstable
   */
  enableAddContentTypeCharset = false;

  /**
   * handler is executed for each incoming request. handler calls hooks at
   * specific points in the handler life-cycle that allow returning a response
   * before a route is matched or entered (onHandlerEnter, onHandlerBeforeRouteEnter),
   * returning a response when no route matched (onHandlerServePublicFile, onHandlerNotFound),
   * and modifying the response (onHandlerExit). Hooks in order:
   * 1. onHandlerEnter
   * 2. onHandlerBeforeRouteEnter
   * 3. onHandlerServePublicFile
   * 4. onHandlerNotFound
   * 5. onHandlerExit
   */
  handler: (
    request: Request,
    connInfo: Deno.ServeHandlerInfo,
  ) => Response | Promise<Response> = async (
    request: Request,
    connInfo: Deno.ServeHandlerInfo,
  ): Promise<Response> => {
    try {
      const url = new URL(request.url);
      const context = await this.#createContext({ connInfo, request, url });
      let response: Response | undefined;

      if (this.onHandlerEnter) {
        response = await this.onHandlerEnter(context);
      }

      if (!response) {
        for (const routeName in this.#routes) {
          const route = this.#routes[routeName];
          let match: URLPatternResult | boolean | null;

          if (typeof route.pattern === "string") {
            match = context.url.pathname === route.pattern;
          } else {
            match = route.pattern.exec(request.url);
            if (match) context.urlPatternResult = match;
          }
          if (match) {
            const requestMethod = HttpMethod.fromString(request.method);

            if (
              !(route.methods & requestMethod) &&
              !(requestMethod === HttpMethod.HEAD && route.methods & HttpMethod.GET)
            ) {
              return new Response("HTTP Method Not Allowed", {
                headers: {
                  allow: Object.values(HttpMethod).flatMap((method) =>
                    typeof method === "number" && (route.methods & method)
                      ? HttpMethod.toString(method)
                      : []
                  ).join(","),
                },
                status: 405,
              });
            }

            if (this.onHandlerBeforeRouteEnter) {
              response = await this.onHandlerBeforeRouteEnter(context, route);
            }
            if (!response) {
              response = await route.handler(context);
            }
            break;
          }
        }
      }
      if (!response && this.#enablePublicDir) {
        response = await this.onHandlerServePublicFile(context);
      }
      if (!response) {
        response = await this.onHandlerNotFound(context);
      }
      if (this.enableAddContentTypeCharset) {
        this.#addContentTypeCharset(response.headers);
      }
      if (this.onHandlerExit) {
        response = await this.onHandlerExit(context, response);
      }
      return response;
    } catch (error: unknown) {
      return this.onError(request, connInfo, error);
    }
  };

  /**
   * onHandlerEnter is called by handler() before searching for a route that
   * matches the request URL. If it returns a response, handler() skips route
   * matching and the file and not found hook. onHandlerEnter is useful for
   * redirecting or blocking requests, e.g. redirecting from www to apex domain
   * or blocking based on IP or user agent.
   */
  onHandlerEnter?: (
    context: Context,
  ) => Promise<Response | undefined> | Response | undefined;

  /**
   * onHandlerBeforeRouteEnter is called by handler() when a route matches the
   * request URL but before the route handler is called. If it returns a
   * response, the route handler is ignored. onHandlerBeforeRouteEnter is useful
   * for guarding routes, e.g. checking access permissions and redirecting to a
   * login page.
   */
  onHandlerBeforeRouteEnter?: (
    context: Context,
    route: Route<Context, RouteMeta>,
  ) => Promise<Response | undefined> | Response | undefined;

  /**
   * onHandlerServePublicFile is called by handler() if the response is
   * undefined at this point in the life-cycle, e.g. when no route matched, and
   * serving files from the public directory is enabled. If a file exists in the
   * public directory at the location specified by the request URL path, it
   * returns a response with the file ready to be transfered in chunks. If no
   * file exists at the location, it returns undefined.
   */
  onHandlerServePublicFile = async (
    context: Context,
  ): Promise<Response | undefined> => {
    try {
      return await retry(async () => {
        try {
          const path = `${this.#publicDir}${context.url.pathname}`;
          const file = await Deno.open(path, { read: true });

          if (!(await file.stat()).isFile) {
            file.close();
            return;
          }

          const extension = path.split(".").at(-1)?.toLowerCase();
          const contentType = extension ? getContentType(extension) : undefined;

          return new Response(file.readable, {
            headers: {
              "cache-control": "immutable,max-age=31536000,public",
              ...(contentType ? { "content-type": contentType } : {}),
            },
          });
        } catch (error: unknown) {
          if (!(error instanceof Deno.errors.NotFound)) {
            throw error;
          }
        }
      });
    } catch (_error: unknown) {
      // Do nothing (error is logged by retry())
    }
  };

  /**
   * handlerNotFound is called by handler() if the response is still undefined
   * at this point in the life-cycle, e.g. neither a route nor a public file
   * matches the request URL. Replace this function to provide a custom 404 Not
   * Found page.
   */
  onHandlerNotFound: Handler<Context> = (_context: Context) => {
    return new Response("Not Found", {
      headers: { "content-type": ContentType.TextPlain },
      status: 404,
    });
  };

  /**
   * onHandlerExit is called by handler() after all other hooks and before
   * serving the response to the client. It is useful for modifying the response
   * headers or logging metrics.
   */
  onHandlerExit?: (
    context: Context,
    response: Response,
  ) => Response | Promise<Response>;

  /**
   * onError is called when an uncaught exception occurs in handler. This function can be replaced
   * to serve a custom 500 Internal Server Error page.
   */
  onError: (
    _request: Request,
    connInfo: Deno.ServeHandlerInfo,
    error: unknown,
  ) => Promise<Response> | Response = (
    _request: Request,
    _connInfo: Deno.ServeHandlerInfo,
    error: unknown,
  ): Response => {
    console.error(error);
    return new Response("Internal Server Error", {
      headers: { "content-type": ContentType.TextPlain },
      status: 500,
    });
  };

  #createContext: (baseContext: BaseContext) => Context | Promise<Context>;

  #enablePublicDir: boolean;

  #publicDir: string;

  #routes: Routes<Context, RouteMeta>;

  constructor(options: {
    createContext: (baseContext: BaseContext) => Context | Promise<Context>;
    enablePublicDir?: boolean;
    publicDir?: string;
    routes?: Routes<Context, RouteMeta>;
  }) {
    this.#createContext = options.createContext;
    this.#enablePublicDir = options.enablePublicDir ?? false;
    this.#publicDir = options.publicDir ?? "public";
    this.#routes = options.routes ?? {};
  }

  #addContentTypeCharset(headers: Headers): void {
    let header = headers.get("content-type");
    if (header && !header.startsWith("text/")) return;
    if (header === "text/plain;charset=UTF-8") header = null;
    const pieces = header?.split(";") ?? [];
    const contentType = pieces.at(0)?.trim() || "text/html";
    const charset = pieces.at(1)?.trim() || "charset=utf-8";
    headers.set("content-type", `${contentType};${charset}`);
  }
}
